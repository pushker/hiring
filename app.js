var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var MongoClient = require('mongodb').MongoClient;
var dotenv = require('dotenv').config();
var cors = require('cors');

// Include Files
var helpers = require('./helpers');
var hiringRouter = require('./routes/index');
var otherHiringRouter = require('./routes/users');
var adminHiringRouter = require('./routes/admin');
var applyJobHiringRouter = require('./routes/applyJob');
var ethereumRouter = require('./routes/ethereum');

// Initialize Express
var app = express();

// Database Connection
var db;
MongoClient.connect(process.env.MONGO_URL, function(err, client) {
    if (err) {
        throw err;
    }
    db = client.db(process.env.MONGO_DATABASE);
});

// Middleware Initialization

// Default Middlewares
app.use(cors());
app.use(logger('dev'));
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb' }));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// JWT Middleware
app.use(async function(req, res, next) {
    if (req.path == '/user/login' || req.path == '/zoho/jobs/all' || req.path == '/zoho/getJobById' || req.path == '/change_password' || req.path == '/reset_password' || req.path == '/reset_password_with_otp' || req.path == '/signup' || req.path == '/countryList' || req.path == '/zoho/jobs/getBySearchCashBack' || req.path == '/user/getJobById' || req.path == '/test/addCashback' || req.path == '/test/receiveCashback' || req.path == '/ether/regenerate-wallet') {
        next();
    } else {
        try {
            var verify = helpers.verifyJWT(req.headers.authorization);
            if (verify) {
                var user = await db.collection('users').findOne({
                    email: verify.email,
                    username: verify.username,
                    type: verify.type
                });

                if (user.email == verify.email && user.username == verify.username && user.type == verify.type) {
                    next();
                } else {
                    res.status(400).send({
                        "status": "error",
                        "params": "",
                        "message": "Access Token Signature Mismatch"
                    });
                }
            } else {
                res.status(400).send({
                    "status": "error",
                    "params": "",
                    "message": "Access Token not Valid - Please Login Again"
                });
            }
        } catch (err) {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": err
            });
        }
    }
});

// Route Middleware
app.use('/', hiringRouter);
app.use('/user', otherHiringRouter);
app.use('/admin', adminHiringRouter);
app.use('/Job', applyJobHiringRouter);
app.use('/ether', ethereumRouter);
app.use('/test', require('./routes/test'));

// App Exporting
module.exports = app;