var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var randomstring = require("randomstring");
var axios = require("axios");
var _ = require("lodash");
var Web3 = require('web3');

//        ___       ________   ____      __                        __  _
//       / / |     / /_  __/  /  _/___  / /____  ____ __________ _/ /_(_)___  ____
//  __  / /| | /| / / / /     / // __ \/ __/ _ \/ __ `/ ___/ __ `/ __/ / __ \/ __ \
// / /_/ / | |/ |/ / / /    _/ // / / / /_/  __/ /_/ / /  / /_/ / /_/ / /_/ / / / /
// \____/  |__/|__/ /_/    /___/_/ /_/\__/\___/\__, /_/   \__,_/\__/_/\____/_/ /_/
//                                            /____/

exports.issueJWT = function (user) {
    var payload = {
        email: user.email,
        username: user.username,
        type: user.type
    }

    var options = {
        audience: process.env.JWT_AUDIENCE,
        expiresIn: process.env.JWT_EXPIRY,
    }

    var jwtToken = jwt.sign(payload, process.env.JWT_KEY, options);
    return jwtToken;
}

exports.verifyJWT = function (bearer) {
    var token = bearer.split(" ")[1];

    var verify = jwt.verify(token, process.env.JWT_KEY, {
        audience: process.env.JWT_AUDIENCE
    });

    return verify;
}

exports.makeId = function () {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 16; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

//     ________  __                                      ______                 __  _
//    / ____/ /_/ /_  ___  ________  __  ______ ___     / ____/_  ______  _____/ /_(_)___  ____  _____
//   / __/ / __/ __ \/ _ \/ ___/ _ \/ / / / __ `__ \   / /_  / / / / __ \/ ___/ __/ / __ \/ __ \/ ___/
//  / /___/ /_/ / / /  __/ /  /  __/ /_/ / / / / / /  / __/ / /_/ / / / / /__/ /_/ / /_/ / / / (__  )
// /_____/\__/_/ /_/\___/_/   \___/\__,_/_/ /_/ /_/  /_/    \__,_/_/ /_/\___/\__/_/\____/_/ /_/____/

var web3 = new Web3(Web3.givenProvider || process.env.ETHEREUM_IP);

exports.createWallet = async function () {
    let hex = exports.randomHex();
    let wallet = await web3.eth.accounts.create(hex);
    return wallet;
}

exports.exportWallet = async function (private_key) {
    var password = exports.createNonce();
    let keystore = await web3.eth.accounts.encrypt(private_key, password);

    var exportWallet = {
        keystore: keystore,
        password: password
    }
    return exportWallet;
}

exports.balance = async function (address) {
    var etherBalance = await web3.eth.getBalance(address);
    var balance = exports.fromWei(etherBalance);
    return balance;
}

exports.estimateFee = async function (from, to, value) {
    let gasPrice = await web3.eth.getGasPrice();

    let inWei = await web3.eth.estimateGas({
        from: from,
        to: to,
        value: exports.toWei(value),
        gasPrice: gasPrice
    });

    let inEth = (exports.toBN(inWei) * exports.toBN(gasPrice)) / Math.pow(10, 18);

    let estimatedFee = {
        inWei: inWei,
        inEth: inEth
    }
    return estimatedFee;
}

exports.transactionEther = async function (from, to, value, fee, key) {
    let signedTx = await web3.eth.accounts.signTransaction({
        from: from,
        to: to,
        value: exports.toWei(value),
        gas: fee.inWei
    }, key);

    return new Promise(function (resolve, reject) {
        web3.eth.sendSignedTransaction(signStatus.rawTransaction, function (error, transactionHash) {
            if (error) {
                reject(error);
            } else {
                console.log(transactionHash);
                resolve(transactionHash);
            }
        });
    });
}

exports.addCashback = async function (name, identifier, wallet, cashbackAmount, cashbackTime) {
    var contract = exports.contract;
    var cashbackContract = new web3.eth.Contract(contract.contractABI, contract.contractAddress);

    var estimatedGas = await cashbackContract.methods.addCashback(name, identifier, wallet, cashbackAmount, cashbackTime).estimateGas({
        from: contract.ownerAddress,
        gas: 5000000
    });

    var data = await cashbackContract.methods.addCashback(name, identifier, wallet, cashbackAmount, cashbackTime).encodeABI();

    let gasPrice = await web3.eth.getGasPrice();
    var signStatus = await web3.eth.accounts.signTransaction({
        "from": contract.ownerAddress,
        "to": contract.contractAddress,
        "gas": estimatedGas,
        "data": data,
        "gasPrice": gasPrice
    }, contract.ownerPrivateKey);

    return new Promise(function (resolve, reject) {
        web3.eth.sendSignedTransaction(signStatus.rawTransaction, function (error, transactionHash) {
            if (error) {
                reject(error);
            } else {
                console.log(transactionHash);
                resolve(transactionHash);
            }
        });
    });
}

exports.receiveCashback = async function (identifier, wallet) {
    var contract = exports.contract;
    var cashbackContract = new web3.eth.Contract(contract.contractABI, contract.contractAddress);

    var estimatedGas = await cashbackContract.methods.receiveCashback(identifier, wallet).estimateGas({
        from: contract.ownerAddress,
        gas: 5000000
    });

    var data = await cashbackContract.methods.receiveCashback(identifier, wallet).encodeABI();

    let gasPrice = await web3.eth.getGasPrice();
    var signStatus = await web3.eth.accounts.signTransaction({
        "from": contract.ownerAddress,
        "to": contract.contractAddress,
        "gas": estimatedGas,
        "data": data,
        "gasPrice": gasPrice
    }, contract.ownerPrivateKey);

    return new Promise(function (resolve, reject) {
        web3.eth.sendSignedTransaction(signStatus.rawTransaction, function (error, transactionHash) {
            if (error) {
                reject(error);
            } else {
                console.log(transactionHash);
                resolve(transactionHash);
            }
        });
    });
}

//     ________  __                                      __  __     __
//    / ____/ /_/ /_  ___  ________  __  ______ ___     / / / /__  / /___  ___  __________
//   / __/ / __/ __ \/ _ \/ ___/ _ \/ / / / __ `__ \   / /_/ / _ \/ / __ \/ _ \/ ___/ ___/
//  / /___/ /_/ / / /  __/ /  /  __/ /_/ / / / / / /  / __  /  __/ / /_/ /  __/ /  (__  )
// /_____/\__/_/ /_/\___/_/   \___/\__,_/_/ /_/ /_/  /_/ /_/\___/_/ .___/\___/_/  /____/
//                                                               /_/

exports.ethereumPrice = async function () {
    var response = await axios.get(process.env.ETHEREUM_PRICE_API);
    var ethereumPrice = {
        inEth: "1",
        inUSD: response.data.data.quotes.USD.price,
        inJPY: response.data.data.quotes.JPY.price
    }
    return ethereumPrice;
}

exports.calculateCashback = async function (yenValue, percentage) {
    var response = await exports.ethereumPrice();
    var price = (parseFloat(yenValue) / response.inJPY) * percentage;
    // var res = _.round((price / 100), 7);
    var res = parseFloat(price / 100);
    console.log(res);
    var wei = exports.toHex(res * Math.pow(10, 18));
    return wei;
}

exports.calculateCashbackAmount = async function (yenValue, percentage) {
    var response = await exports.ethereumPrice();
    var price = (parseInt(yenValue) / response.inJPY) * parseInt(percentage);
    var res = parseFloat(price / 100);
    return res.toString();
}

exports.totalEtherCashback = async function (yenValue) {
    var response = await exports.ethereumPrice();
    var price = parseFloat(yenValue) / response.inJPY;
    return price.toString();
}

exports.createNonce = function () {
    var nonce = randomstring.generate({
        length: 16,
        charset: 'hex'
    });
    return nonce;
}

exports.randomHex = function () {
    let hex = web3.utils.randomHex(32);
    return hex;
}

exports.toWei = function (balance) {
    let wei = web3.utils.toWei(balance);
    return _.round(wei, 8);
}

exports.fromWei = function (balance) {
    let wei = web3.utils.fromWei(balance);
    return _.round(wei, 8);
}

exports.toHex = function (number) {
    let hex = web3.utils.toHex(number);
    return hex;
}

exports.toBN = function (number) {
    let bn = web3.utils.toBN(number);
    return bn;
}

exports.toStr = function (number) {
    let string = number.toString();
    return string;
}

//    ______            __                  __     ____       __        _ __
//   / ____/___  ____  / /__________ ______/ /_   / __ \___  / /_____ _(_) /____
//  / /   / __ \/ __ \/ __/ ___/ __ `/ ___/ __/  / / / / _ \/ __/ __ `/ / / ___/
// / /___/ /_/ / / / / /_/ /  / /_/ / /__/ /_   / /_/ /  __/ /_/ /_/ / / (__  )
// \____/\____/_/ /_/\__/_/   \__,_/\___/\__/  /_____/\___/\__/\__,_/_/_/____/

exports.contract = {
    "ownerAddress": "0x5F521C81C4569F18a11fae67621Af6a979AD62fB",
    "ownerPrivateKey": "0x647a4f270b0d810a0e69807c8e4557d3fc2d245569e7e5371a708e6606a82654",
    "contractAddress": "0xd94002814ff4C8d32965da5c2833BF9C34CEB46B",
    "contractABI": [
        {
            "constant": true,
            "inputs": [
                {
                    "name": "",
                    "type": "uint256"
                },
                {
                    "name": "",
                    "type": "address"
                }
            ],
            "name": "userDetails",
            "outputs": [
                {
                    "name": "name",
                    "type": "string"
                },
                {
                    "name": "wallet",
                    "type": "address"
                },
                {
                    "name": "cashbackAmount1",
                    "type": "uint256"
                },
                {
                    "name": "cashbackTime1",
                    "type": "uint256"
                },
                {
                    "name": "cashbackPaid1",
                    "type": "bool"
                },
                {
                    "name": "cashbackAmount2",
                    "type": "uint256"
                },
                {
                    "name": "cashbackTime2",
                    "type": "uint256"
                },
                {
                    "name": "cashbackPaid2",
                    "type": "bool"
                },
                {
                    "name": "cashbackAmount3",
                    "type": "uint256"
                },
                {
                    "name": "cashbackTime3",
                    "type": "uint256"
                },
                {
                    "name": "cashbackPaid3",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "identifier",
                    "type": "uint256"
                },
                {
                    "name": "wallet",
                    "type": "address"
                }
            ],
            "name": "receiveCashback",
            "outputs": [
                {
                    "name": "success",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [],
            "name": "owner",
            "outputs": [
                {
                    "name": "",
                    "type": "address"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "name",
                    "type": "string"
                },
                {
                    "name": "identifier",
                    "type": "uint256"
                },
                {
                    "name": "wallet",
                    "type": "address"
                },
                {
                    "name": "cashbackAmount",
                    "type": "uint256[]"
                },
                {
                    "name": "cashbackTime",
                    "type": "uint256[]"
                }
            ],
            "name": "addCashback",
            "outputs": [
                {
                    "name": "success",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [],
            "name": "balance",
            "outputs": [
                {
                    "name": "",
                    "type": "uint256"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "payable": true,
            "stateMutability": "payable",
            "type": "fallback"
        },
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": false,
                    "name": "name",
                    "type": "string"
                },
                {
                    "indexed": true,
                    "name": "wallet",
                    "type": "address"
                }
            ],
            "name": "CashbackAdded",
            "type": "event"
        },
        {
            "anonymous": false,
            "inputs": [
                {
                    "indexed": true,
                    "name": "wallet",
                    "type": "address"
                },
                {
                    "indexed": false,
                    "name": "cashback",
                    "type": "uint256"
                }
            ],
            "name": "CashbackReceived",
            "type": "event"
        }
    ]
}