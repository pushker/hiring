var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var jwt = require('jsonwebtoken');
var axios = require('axios');
var _ = require('lodash');
var helpers = require('../helpers');
var ObjectId = require('mongodb').ObjectId;
var moment = require('moment');

var nodemailer = require('nodemailer');
const util = require('util')
const options = {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        user: 'dotnet.rev@gmail.com',
        pass: '#rev#123'
    }
};
const transporter = nodemailer.createTransport(options);
//     ____        __        __                       ______                            __  _
//    / __ \____ _/ /_____ _/ /_  ____ _________     / ____/___  ____  ____  ___  _____/ /_(_)___  ____
//   / / / / __ `/ __/ __ `/ __ \/ __ `/ ___/ _ \   / /   / __ \/ __ \/ __ \/ _ \/ ___/ __/ / __ \/ __ \
//  / /_/ / /_/ / /_/ /_/ / /_/ / /_/ (__  )  __/  / /___/ /_/ / / / / / / /  __/ /__/ /_/ / /_/ / / / /
// /_____/\__,_/\__/\__,_/_.___/\__,_/____/\___/   \____/\____/_/ /_/_/ /_/\___/\___/\__/_/\____/_/ /_/

var db;
MongoClient.connect(process.env.MONGO_URL, function (err, client) {
    if (err) {
        throw err;
    }
    db = client.db(process.env.MONGO_DATABASE);
});

//     ____              __
//    / __ \____  __  __/ /____  _____
//   / /_/ / __ \/ / / / __/ _ \/ ___/
//  / _, _/ /_/ / /_/ / /_/  __(__  )
// /_/ |_|\____/\__,_/\__/\___/____/

router.post('/progressJob', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var creds = req.body;
        var validId = ObjectId.isValid(creds._id);
        if (validId == true) {
            db.collection('appliedJobs').find({
                '_id': ObjectId(creds._id)
            }).toArray(function (err, resp) {
                if (err) {
                    res.status(400).send({
                        "status": "error",
                        "message": "No appliedJobs found for that ID"
                    });
                } else {
                    db.collection('appliedJobs').update({
                        '_id': ObjectId(creds._id),
                    }, {
                            $set: {
                                "progress": creds.progress,
                            }
                        }, function (error, response) {
                            if (error) {
                                res.status(400).send({
                                    "status": "error",
                                    "message": "Something bad happened.Please try later."
                                });
                            } else {
                                res.status(200).send({
                                    "status": "ok",
                                    "message": "AppliedJob Applicant status changed successfully."
                                });
                            }
                        })
                }
            })
        } else {
            res.status(400).send({
                "status": "error",
                "params": ObjectId.isValid(creds._id),
                "message": "AppliedJob ID is not valid ID"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

router.post('/setSchedule', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var creds = req.body;
        var validId = ObjectId.isValid(creds._id);
        if (validId == true) {
            db.collection('appliedJobs').find({
                '_id': ObjectId(creds._id)
            }).toArray(function (err, resp) {
                if (err) {
                    res.status(400).send({
                        "status": "error",
                        "message": "No appliedJobs found for that ID"
                    });
                } else {
                    var date = creds.date.year + '-' + creds.date.month + '-' + creds.date.day;
                    var dateQuery = moment(date, "YYYY-MM-DD");
                    var timeFormat1 = moment(creds.time, 'hh:mm A');
                    timeFormat = moment(timeFormat1).format('hh:mm a');
                    db.collection('appliedJobs').update({
                        '_id': ObjectId(creds._id),
                    }, {
                            $set: {
                                "progress": "accept",
                                "interviewDetail": {
                                    "date": dateQuery,
                                    "time": timeFormat,
                                    "place": creds.place,
                                    "document": creds.document
                                }
                            }
                        }, function (error, response) {
                            if (error) {
                                res.status(400).send({
                                    "status": "error",
                                    "message": "Something bad happened.Please try later."
                                });
                            } else {
                                res.status(200).send({
                                    "status": "ok",
                                    "message": "AppliedJob Applicant status changed successfully."
                                });
                            }
                        })
                }
            })
        } else {
            res.status(400).send({
                "status": "error",
                "params": ObjectId.isValid(creds._id),
                "message": "AppliedJob ID is not valid ID"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

router.post('/hiring', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var creds = req.body;
        if (creds.apjId) {
            var dataCheck = await db.collection('appliedJobs').find({
                '_id': ObjectId(creds.apjId),
                // 'jobDetail.start_sal': { "$lte": parseInt(creds.salary) },
                // 'jobDetail.end_sal': { "$gte": parseInt(creds.salary) },
                'jobDetail.start_cashback': { "$lte": parseInt(creds.cashback) },
                'jobDetail.end_cashback': { "$gte": parseInt(creds.cashback) }
            }).toArray();
            if (dataCheck.length > 0) {
                var updateJobStatus = await db.collection('appliedJobs').update({
                    "_id": ObjectId(creds.apjId)
                }, {
                        $set: {
                            "progress": "job_offfer_send"
                        }

                    });
                if (updateJobStatus) {
                    var dateCashback_date1 = req.body.cashback_date1.year + '-' + req.body.cashback_date1.month + '-' + req.body.cashback_date1.day;
                    var dateCashback_date2 = req.body.cashback_date2.year + '-' + req.body.cashback_date2.month + '-' + req.body.cashback_date2.day;
                    var dateCashback_date3 = req.body.cashback_date3.year + '-' + req.body.cashback_date3.month + '-' + req.body.cashback_date3.day;
                    req.body.cashback_date1 = moment(dateCashback_date1, "YYYY-MM-DD");
                    req.body.cashback_date1 = req.body.cashback_date1._d;
                    req.body.cashback_date2 = moment(dateCashback_date2, "YYYY-MM-DD");
                    req.body.cashback_date2 = req.body.cashback_date2._d;
                    req.body.cashback_date3 = moment(dateCashback_date3, "YYYY-MM-DD");
                    req.body.cashback_date3 = req.body.cashback_date3._d;

                    req.body['cashback_status1'] = false;
                    req.body['cashback_status2'] = false;
                    req.body['cashback_status3'] = false;
                    req.body['status'] = '1';
                    req.body['totalCashback'] = dataCheck[0]['jobDetail']['cashback'];
                    var ethercash1 = await helpers.calculateCashback(req.body.cashback, req.body.cashback_amount1);
                    var ethercash2 = await helpers.calculateCashback(req.body.cashback, req.body.cashback_amount2);
                    var ethercash3 = await helpers.calculateCashback(req.body.cashback, req.body.cashback_amount3);
                    var etherAmount1 = await helpers.calculateCashbackAmount(req.body.cashback, req.body.cashback_amount1);
                    var etherAmount2 = await helpers.calculateCashbackAmount(req.body.cashback, req.body.cashback_amount2);
                    var etherAmount3 = await helpers.calculateCashbackAmount(req.body.cashback, req.body.cashback_amount3);
                    req.body['cashbackEther'] = await helpers.totalEtherCashback(req.body.cashback);
                    req.body['arrayCash'] = [];
                    console.log(ethercash1, "asfsa");
                    req.body['arrayCash'].push(ethercash1);
                    req.body['arrayCash'].push(ethercash2);
                    req.body['arrayCash'].push(ethercash3);
                    var apjIdHex = await helpers.toHex(req.body.apjId);
                    req.body['apjIdHex'] = apjIdHex;
                    var etherDate1 = moment(req.body.cashback_date1).format("X");
                    var etherDate2 = moment(req.body.cashback_date2).format("X");
                    var etherDate3 = moment(req.body.cashback_date3).format("X");
                    req.body['arrayCashDates'] = [etherDate1, etherDate2, etherDate3];
                    req.body['arrayEthers'] = [etherAmount1, etherAmount2, etherAmount3];

                    try {
                        var addCashback = await helpers.addCashback(req.body.name, apjIdHex, req.body.address, req.body['arrayCash'], req.body['arrayCashDates']);

                        req.body['addCashback_hash'] = addCashback;
                        req.body['created_at'] = moment().format("YYYY-MM-DD");
                        req.body['receive_hash'] = {
                            "hash_1": "",
                            "hash_2": "",
                            "hash_3": ""
                        }

                        var result_data = await db.collection('hiringjobs').insert(req.body);
                        var appliedJobStatus = await db.collection('appliedJobs').update({
                            "_id": ObjectId(creds.apjId)
                        }, {
                                $set: {
                                    "initiate": 1
                                }
                            });
                        if (result_data) {
                            res.status(200).send({
                                "status": "ok",
                                "params": "",
                                "message": "Hiring details saved successfully."
                            });

                        } else {
                            res.status(400).send({
                                "status": "error",
                                "message": "Something bad happened.Please try later."
                            });
                        }
                    } catch (error) {
                        res.status(400).send({
                            "status": "error",
                            "message": error
                        });
                    }
                } else {
                    res.status(400).send({
                        "status": "error",
                        "message": "Something bad happened.Please try later."
                    });
                }

            } else {
                res.status(400).send({
                    "status": "error",
                    "message": "Entered Cashback is not in the range"
                });
            }
        } else {
            res.status(400).send({
                "status": "error",
                "message": "No appliedJobs found for that ID"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});
/**
 *
 * change status of appliedJob on user accept and reject the job
 *
 */

router.post('/userChangeJobStatus', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var creds = req.body;
        var validId = ObjectId.isValid(creds._id);
        if (validId == true) {
            var response = await db.collection('appliedJobs').find({
                '_id': ObjectId(creds._id)
            }).toArray();
            if (response.length > 0) {
                var result = await db.collection('appliedJobs').update({
                    '_id': ObjectId(creds._id)
                }, {
                        $set: {
                            "progress": creds.userStatusJob
                        }
                    });
                if (result) {
                    res.status(200).send({
                        "status": "ok",
                        "message": "Job Status updated !!"
                    });
                } else {
                    res.status(400).send({
                        "status": "error",
                        "message": "Something bad happened.Please try later."
                    });
                }
            } else {
                res.status(400).send({
                    "status": "error",
                    "message": "No appliedJobs found for that ID"
                });
            }
        } else {
            res.status(400).send({
                "status": "error",
                "message": "No appliedJobs found for that ID"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

router.post('/sentUserMail', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var creds = req.body;
        if (creds.to) {
            db.collection('users').find({ "email": creds.to }).toArray(function (e, user) {
                if (user.length == 0) {
                    res.status(400).send({
                        "status": "error",
                        "message": "User not found"
                    });
                } else {

                    transporter.sendMail({
                        from: "dotnet.rev@gmail.com",
                        to: creds.to,
                        // to: 'aseth200@gmail.com',
                        subject: creds.subject,
                        html: `<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<style type="text/css">

	/* GENERAL EMAIL CLIENT FIXES */
	#outlook a{padding:0;}
	.ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
	.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
	body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}
	table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
	table{border-collapse:collapse !important; table-layout:fixed; !important; margin: 0 auto; !important;}
	table table {table-layout: auto;}
	img{-ms-interpolation-mode:bicubic;}
	body{margin:0; padding:0;}
	img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}

	/* NO MORE BLUE LINKS ON IPHONE */
	.appleLinks a{color: #4B4C4C;text-decoration: none;}
	.appleLinksWhite a{color: #FFFFFF;text-decoration: none;}

	/* MEDIA QUERY SCREEN 414px */
	@-ms-viewport{width:device-width}
	@media only screen and (max-width:414px){
		table[class="container"]{width:100% !important;}
		td[class="text-center"]{text-align: center !important; width:100% !important;}
		td[class="hide"]{display: none !important;}
		table[class="button-width"]{width:100% !important;}
		img[class="crop"]{height:auto !important; max-width:600px !important; width:100% !important;}
	}
	@media screen and (min-width: 601px) {
		table[class="containerDesktop"]{width: 600px!important;}
	}

	@media (min-width: 400px) and (max-width: 418px){
		.innertablesec{
			width: 150%;
		}
	}

</style>
<title>
</title>
</head>
<body>

	<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td style="text-align: center; font-size: 0px; vertical-align: top;">
					<table align="center" border="0" cellspacing="0" cellpadding="0">
						<tbody>
							<tr>
								<td align="center" valign="middle">
									<a>
										<img src="https://s3.amazonaws.com/demodumps/18342a1a-badd-49d2-9750-f552035094de.png" style="width: 100%;">
									</a>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>

	<!-- ADDITIONAL RESOURCES STARTS HERE -->
	<table width="100%" align="center" bgcolor="#ffffff" border="0" cellspacing="0" cellpadding="0" class="innertablesec">
		<tbody>
			<tr>
				<td align="left" valign="top">
	                <table width="100%" align="center" class="containerDesktop" style="max-width: 100%;background: #fff;height:60vh;" border="0" cellspacing="0" cellpadding="0">
	                	<tbody><br>
		            		<tr>
		            			<td align="left" valign="top" style="padding: 0px 20px 20px; color: rgb(75, 76, 76); line-height: 22px !important; font-family: Arial, Helvetica, sans-serif; font-size: 16px;">
		            				<div class="mktEditable" id="Paragraph">
		            					<table width="560" align="center" class="container" border="0" cellspacing="0" cellpadding="0">
		            						<tbody>
	            								<tr>
	            									<td align="left" valign="top" style="color: rgb(75, 76, 76); line-height: 22px; font-family: Arial, Helvetica, sans-serif; font-size: 16px;padding-top: 10%;">
	            									<strong style="font-size: 24px;">Dear ` + user[0].username + `,</strong>
	            									<br>
	            									<p style="font-size: 30px;line-height: 1.5;">`+ creds.message + `</p>

	            									</td>
	            								</tr>
	            							</tbody>
	            						</table>
	            					</div>
	            				</td>
	            			</tr>
							<tr>
								<table align="center" valign="middle" border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td align="center" valign="top" style="font-family: Arial, Helvetica, sans-serif;">
												<div class="mktEditable" id="Footer">
													<table width="100%" align="center" bgcolor="#000" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr align="center" bgcolor="#000">
																<td><br>
																	<p style="color: #fff;">© Ethereum Hiring 2018. All rights reserved. Terms &amp; condition</p>
																	<hr style="width: 50%;">
																</td>
															</tr>
															<tr>
																<td align="center" bgcolor="#000">
																	<table width="600" align="center" class="container" border="0" cellspacing="0" cellpadding="0">
																	<tbody>
																		<tr>
																			<td align="center" valign="middle" style="padding: 16px 20px 10px;">
																				<table align="center" border="0" cellspacing="0" cellpadding="0">
																					<tbody style="text-align: center;">
																					</tbody>
																				</table>
																			</td>
																		</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</tr>
	        			</tbody>
	        		</table>
				</td>
			</tr>
		</tbody>
	</table>


</body>
</html>`
                    }, function (err, resp) {
                        if (err) {
                            res.status(400).send(err);
                        } else {
                            db.collection('message').insert({
                                "to": creds.to,
                                "from": tokenData.email,
                                "subject": creds.subject,
                                "message": creds.message,
                                "timeStamp": new Date(Date.now())
                            }, function (error, re) {
                                if (error) {
                                    res.status(400).send({
                                        "status": "error",
                                        "message": "Something bad happened.Please try later."
                                    });
                                } else {
                                    res.status(200).send({
                                        "status": "ok",
                                        "params": '',
                                        "message": "Message sent successfully."
                                    });
                                }
                            });
                        }
                    })
                }

            })
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Empty email Address given !!"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

router.get('/email_listing', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var email = req.query.email;
        var creds = req.query;
        if (email) {
            var paginate = 0;
            if (creds['number']) {
                paginate = (creds['number'] - 1) * 10; //skip number of entry
            }
            var records = await db.collection('message').find({
                "to": email
            }).sort({ $natural: -1 }).limit(7).toArray();
            var totalCount = records.length;
            if (totalCount > 0) {
                res.status(200).send({
                    status: "ok",
                    params: {
                        "records": records,
                        "totalCount": totalCount
                    },
                    message: "Successfully fetched email"
                });
            } else {
                res.status(400).send({
                    status: "error",
                    message: "Something went wrong !!"
                })
            }


        } else {
            res.status(400).send({
                status: "error",
                message: "Something went wrong !!"
            })
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

router.get('/userChat', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var creds = req.query;
        if (creds.to && creds.from) {
            var query = {
                $match: {
                    $and: [{
                        $or: [{
                            $and: [{
                                "to": creds.to
                            }, { "from": creds.from }]
                        },
                        {
                            $and: [{
                                "to": creds.from
                            }, { "from": creds.to }]
                        }]
                    }]
                }
            };

            var chat = await db.collection('message').aggregate([query]).toArray();
            var user = await db.collection('users').find({ "email": creds.from }).toArray();
            if (chat.length > 0) {
                res.status(200).send({
                    status: "ok",
                    params: { "chat": chat, "user": user },
                    message: "chat fetched successfully"
                })
            } else {
                res.status(400).send({
                    status: "error",
                    message: "Not found any chat"
                })
            }
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Something bad happened.Please try later"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

router.post('/receiveCashback', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var creds = req.body;
        var email = tokenData.email;
        if (creds.apjId && creds._id) {
            var hiring = await db.collection('hiringjobs').find({
                "apjId": creds.apjId, "_id": ObjectId(creds._id)
            }).toArray();
            if (hiring.length > 0) {
                var updates_status = '';
                if (hiring[0].status == "1") {
                    updates_status = "2";
                    var cashback_status1 = true;
                    var cashback_status2 = false;
                    var cashback_status3 = false;
                } else if (hiring[0].status == "2") {
                    updates_status = "3";
                    cashback_status1 = true;
                    cashback_status2 = true;
                    cashback_status3 = false;
                } else if (hiring[0].status == "3") {
                    updates_status = "completed";
                    cashback_status1 = true;
                    cashback_status2 = true;
                    cashback_status3 = true;
                }
                console.log(hiring[0].apjIdHex, "dsgdsgds", hiring[0].address);
                try {
                    var cashHashCode = await helpers.receiveCashback(hiring[0].apjIdHex, hiring[0].address);
                    if (hiring[0].status == "1") {
                        var first_hash = await db.collection('hiringjobs').update({
                            "apjId": creds.apjId, "_id": ObjectId(creds._id)
                        }, {
                                $set: {
                                    "status": updates_status,
                                    "cashback_status1": cashback_status1,
                                    "cashback_status2": cashback_status2,
                                    "cashback_status3": cashback_status3,
                                    "receive_hash.hash_1": cashHashCode,
                                    "updated_at1": moment().format("YYYY-MM-DD")

                                }
                            });
                    } else if (hiring[0].status == "2") {
                        first_hash = await db.collection('hiringjobs').update({
                            "apjId": creds.apjId, "_id": ObjectId(creds._id)
                        }, {
                                $set: {
                                    "status": updates_status,
                                    "cashback_status1": cashback_status1,
                                    "cashback_status2": cashback_status2,
                                    "cashback_status3": cashback_status3,
                                    "receive_hash.hash_2": cashHashCode,
                                    "updated_at2": moment().format("YYYY-MM-DD")

                                }
                            });
                    } else if (hiring[0].status == "3") {
                        first_hash = await db.collection('hiringjobs').update({
                            "apjId": creds.apjId, "_id": ObjectId(creds._id)
                        }, {
                                $set: {
                                    "status": updates_status,
                                    "cashback_status1": cashback_status1,
                                    "cashback_status2": cashback_status2,
                                    "cashback_status3": cashback_status3,
                                    "receive_hash.hash_3": cashHashCode,
                                    "updated_at3": moment().format("YYYY-MM-DD")

                                }
                            });
                    }
                    res.status(200).send({
                        status: "ok",
                        params: { "hash": cashHashCode },
                        message: "updated your cashback details in contract"
                    })

                } catch (error) {
                    res.status(400).send({
                        "status": "error",
                        "message": error.message
                    });
                }

            } else {
                res.status(400).send({
                    status: "error",
                    message: "Unable to find your Hiring Job Id"
                })
            }
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Something bad happened.Please try later"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

/* apply job listing*/
module.exports = router;

function updateStatus(asd, asd, asd) {

}