var express = require('express');
var router = express.Router();
var helpers = require('../helpers')

// "Anurag Makol","0xe2566b71f029bc11f8d0c54b32fcf6bb88f00cdf",["0x16345785d8a0000", "0x2c68af0bb140000", "0x429d069189e0000"],[1529476151, 1529478151, 1529496151]
router.post('/addCashback', async function (req, res, next) {
    try {
        var cashback = await helpers.addCashback(req.body.name, req.body.wallet, req.body.cashbackAmount, req.body.cashbackTime);
        res.status(200).send({
            "status": "ok",
            "params": cashback
        });
    } catch (err) {
        res.status(400).send({
            "status": "error",
            "params": err
        });
    }
});

// "0xe2566b71f029bc11f8d0c54b32fcf6bb88f00cdf","0x4c1c48947e1a74240e350b6578f17c43a2315d90f7b252db0b8d9b45ee801683"
router.post('/receiveCashback', async function (req, res, next) {
    try {
        var cashback = await helpers.receiveCashback(req.body.wallet, req.body.key);
        res.status(200).send({
            "status": "ok",
            "params": cashback
        });
    } catch (err) {
        res.status(200).send({
            "status": "error",
            "params": err
        });
    }
});

module.exports = router;