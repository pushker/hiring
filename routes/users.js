var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var jwt = require('jsonwebtoken');
var axios = require('axios');
var _ = require('lodash');
var helpers = require('../helpers');
var ObjectId = require('mongodb').ObjectId;
var fs = require('fs');

//     ____        __        __                       ______                            __  _
//    / __ \____ _/ /_____ _/ /_  ____ _________     / ____/___  ____  ____  ___  _____/ /_(_)___  ____
//   / / / / __ `/ __/ __ `/ __ \/ __ `/ ___/ _ \   / /   / __ \/ __ \/ __ \/ _ \/ ___/ __/ / __ \/ __ \
//  / /_/ / /_/ / /_/ /_/ / /_/ / /_/ (__  )  __/  / /___/ /_/ / / / / / / /  __/ /__/ /_/ / /_/ / / / /
// /_____/\__,_/\__/\__,_/_.___/\__,_/____/\___/   \____/\____/_/ /_/_/ /_/\___/\___/\__/_/\____/_/ /_/

var db;
MongoClient.connect(process.env.MONGO_URL, function (err, client) {
    if (err) {
        throw err;
    }
    db = client.db(process.env.MONGO_DATABASE);
});

//     ____              __
//    / __ \____  __  __/ /____  _____
//   / /_/ / __ \/ / / / __/ _ \/ ___/
//  / _, _/ /_/ / /_/ / /_/  __(__  )
// /_/ |_|\____/\__,_/\__/\___/____/

// update cashback in existing job
router.post('/job/update', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var cashback = await db.collection('jobs').update({ '_id': ObjectId(req.body.jobId) }, {
            $set: {
                'cashback': req.body.from + "~" + req.body.to,
                'start_cashback': parseInt(req.body.from),
                'end_cashback': parseInt(req.body.to)
            }
        });

        var update_appliedJob = await db.collection('appliedJobs').find({ 'jobId': req.body.jobId }).toArray();
        if (update_appliedJob.length > 0) {
            for (let index = 0; index < update_appliedJob.length; index++) {
                if (update_appliedJob[index]['jobDetail']['start_cashback'] !== '' && update_appliedJob[index]['jobDetail']['start_cashback'] !== undefined) {

                } else {
                    var dataUpdate = await db.collection('appliedJobs').update({
                        'jobId': req.body.jobId
                    }, {
                            $set: {
                                "jobDetail.start_cashback": parseInt(req.body.from),
                                "jobDetail.end_cashback": parseInt(req.body.to),
                                'jobDetail.cashback': req.body.from + "~" + req.body.to
                            }
                        });
                }
            }
        }


        if (cashback) {
            res.status(200).send({
                "status": "ok",
                "params": "",
                "message": "Cashback Successfully Updated"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Cashback could not be updated"
            });
        }
    } else {
        console.log('session not set');
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

// user apply job
router.post('/applyjob', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var job = await db.collection('appliedJobs').find({ 'jobId': req.body.jobId, 'userId': req.body.userId }).toArray();
        var jobId = await db.collection('jobs').find({ '_id': ObjectId(req.body.jobId) }, { _id: 0 }).toArray();
        var check_wallet_address = await db.collection('wallets').find({ "email": tokenData.email }).toArray();
        console.log(check_wallet_address[0]);
        var user = await db.collection('users').find({ "email": tokenData.email }).toArray();
        if (jobId.length > 0) {
            if (job.length > 0) {
                res.status(400).send({
                    "status": "error",
                    "params": "",
                    "message": "You have already applied this job"
                });
            } else {
                if (check_wallet_address[0] !== '' && check_wallet_address[0] !== undefined) {
                    if ((user[0].resume1 !== '' && user[0].resume1 !== undefined) || (user[0].resume2 !== '' && user[0].resume2 !== undefined)) {
                        req.body['title'] = jobId[0]['Posting Title'];
                        req.body['jobDetail'] = jobId[0];
                        req.body['firstname'] = user[0].firstname;
                        req.body['lastname'] = user[0].lastname;
                        req.body['username'] = user[0].username;
                        req.body['initiate'] = 0;
                        var data = await db.collection('appliedJobs').insert(req.body);
                        if (data) {
                            res.status(200).send({
                                "status": "ok",
                                "params": "",
                                "message": "Successfully Applied"
                            });
                        } else {
                            res.status(400).send({
                                status: "error",
                                message: "Sorry, unable to process your request"
                            })
                        }

                    } else {
                        res.status(400).send({
                            status: "error",
                            message: "Sorry,Your application does not process, firstly upload any one of resume in profile section !!"
                        });
                    }
                } else {
                    res.status(400).send({
                        "status": "error",
                        "message": "Please first add Ethereum address under profile page before to apply a job."
                    });
                }

            }

        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "No jobs exist for this ID."
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

//get job detail by job id
router.get('/zoho/getJobById', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var job = await db.collection('jobs').findOne({ '_id': ObjectId(req.query['id']) });
        if (job) {
            res.status(200).send({
                "status": "ok",
                "params": job,
                "message": "Job Fetched Successfully"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Job not found"
            });
        }
    } else {
        console.log('session not set');
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

//without token show job
router.get('/getJobById', async function (req, res, next) {
    var id = req.query['id'];
    if (id) {
        var job = await db.collection('jobs').findOne({ '_id': ObjectId(req.query['id']) });
        if (job) {
            res.status(200).send({
                "status": "ok",
                "params": job,
                "message": "Job Fetched Successfully"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Job not found"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Job not found"
        });
    }
});


/* user listing*/
//get user by user type
router.post('/getAllUsers', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        if (tokenData.type == 'Administrator') {
            var query = {};
            if (req.body.search_user) {
                var regex1 = new RegExp(req.body.search_user, "i")
                query = {
                    'type': req.body.filterBy,
                    $or: [{
                        'username': { $regex: regex1, $options: 'i' }
                    }, {
                        'firstname': { $regex: regex1, $options: 'i' }
                    }, {
                        'lastname': { $regex: regex1, $options: 'i' }
                    }, {
                        'email': { $regex: regex1, $options: 'i' }
                    }]
                }
            } else {
                query = {
                    'type': req.body.filterBy
                }
            }
            var totalUsers = await db.collection('users').find(query).count();
            var paginate = (req.body.index - 1) * req.body.limit;
            var users = await db.collection('users').find(query).skip(paginate).limit(req.body.limit).sort({ $natural: -1 }).toArray();

            if (users) {
                res.status(200).send({
                    "status": "ok",
                    "params": {
                        users: users,
                        totalUsers: totalUsers
                    },
                    "message": "Users Fetched Successfully"
                });
            } else {
                res.status(400).send({
                    "status": "error",
                    "params": "",
                    "message": "Users could not be fetched"
                });
            }
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "You have not the authority to access this page"
            });
        }

    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

router.post('/changeRole', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var user = await db.collection('users').update({ '_id': ObjectId(req.body.userId) }, {
            $set: {
                'type': req.body.type
            }
        });
        if (user) {
            res.status(200).send({
                "status": "ok",
                "params": "",
                "message": "User Role Updated Successfully"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "User role could not be updated"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

router.post('/changeStatus', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var user = await db.collection('users').update({ '_id': ObjectId(req.body.userId) }, {
            $set: {
                'status': req.body.status
            }
        });
        if (user) {
            res.status(200).send({
                "status": "ok",
                "params": "",
                "message": "User Status Updated Successfully"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "User status could not be updated"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});
router.get('/getAppliedJobById', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var jobTitle = await db.collection('appliedJobs').distinct("title");
        if (jobTitle.length > 0) {
            res.status(200).send({
                "status": "ok",
                "params": jobTitle,
                "message": "Titles got Successfully"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "error got"
            });
        }

    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }

});
//global search
router.post('/globalSearch', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var paginate = (req.body.number - 1) * 12; //skip number of entry
        var data = {};
        if (req.body.search_cash || req.body.search_salary) {
            data['$and'] = [];
        }
        if (req.body.search_key) {
            data['$or'] = [];
        }
        if (req.body.search_cash) {
            data['$and'].push({ 'start_cashback': { '$gte': parseInt(req.body.search_cash.split('~')[0]) }, 'end_cashback': { '$lte': parseInt(req.body.search_cash.split('~')[1]) } });
        }
        if (req.body.search_salary) {
            data['$and'].push({ 'start_sal': { '$gte': parseInt(req.body.search_salary.split('~')[0]) }, 'end_sal': { '$lte': parseInt(req.body.search_salary.split('~')[1]) } });
        }
        if (req.body.search_key) {
            data['$or'].push({ 'Job Opening ID': { $regex: req.body.search_key, $options: 'i' } }, { 'Date Opened': { $regex: req.body.search_key, $options: 'i' } }, { 'Posting Title': { $regex: req.body.search_key, $options: 'i' } }, { 'Required Japanese level': { $regex: req.body.search_key, $options: 'i' } }, { 'Job Type': { $regex: req.body.search_key, $options: 'i' } }, { 'City': { $regex: req.body.search_key, $options: 'i' } });
        }

        var totalJobs = await db.collection('jobs').find(data).count();
        var jobs = await db.collection('jobs').find(data).skip(paginate).limit(12).toArray();
        if (jobs) {
            res.status(200).send({
                "status": "ok",
                "params": {
                    'jobs': jobs,
                    'totalJobs': totalJobs
                },
                "message": "Jobs Fetched Successfully"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Jobs could not be fetched"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

//

exports.uploadDoc = async function (document, extension) {
    try {
        var Eresume = document;
        let buff = await new Buffer(Eresume.toString('utf8'), 'base64');
        var filename = await Date.now() + "_" + extension;
        var file = await fs.writeFileSync(process.env.FILEPATH + "/" + filename, buff);
        return process.env.WEBURL + "/" + filename;
    } catch (error) {
        return "Corruted Document";
    }
}
/* user listing*/
module.exports = router;