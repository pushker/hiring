var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var jwt = require('jsonwebtoken');
var axios = require('axios');
var _ = require('lodash');
var helpers = require('../helpers');
var ObjectId = require('mongodb').ObjectId;
var nodemailer = require('nodemailer');
const util = require('util');
var fs = require('fs');
const options = {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        user: 'dotnet.rev@gmail.com',
        pass: '#rev#123'
    }
};
const transporter = nodemailer.createTransport(options);
//     ____        __        __                       ______                            __  _
//    / __ \____ _/ /_____ _/ /_  ____ _________     / ____/___  ____  ____  ___  _____/ /_(_)___  ____
//   / / / / __ `/ __/ __ `/ __ \/ __ `/ ___/ _ \   / /   / __ \/ __ \/ __ \/ _ \/ ___/ __/ / __ \/ __ \
//  / /_/ / /_/ / /_/ /_/ / /_/ / /_/ (__  )  __/  / /___/ /_/ / / / / / / /  __/ /__/ /_/ / /_/ / / / /
// /_____/\__,_/\__/\__,_/_.___/\__,_/____/\___/   \____/\____/_/ /_/_/ /_/\___/\___/\__/_/\____/_/ /_/

var db;
MongoClient.connect(process.env.MONGO_URL, function(err, client) {
    if (err) {
        throw err;
    }
    db = client.db(process.env.MONGO_DATABASE);
});

//     ____              __
//    / __ \____  __  __/ /____  _____
//   / /_/ / __ \/ / / / __/ _ \/ ___/
//  / _, _/ /_/ / /_/ / /_/  __(__  )
// /_/ |_|\____/\__,_/\__/\___/____/
router.post('/user/login', async function(req, res, next) {
    var user = await db.collection('users').findOne({
        email: req.body.email,
        password: req.body.password
    });

    if (user) {
        if (user.status == true) {
            var jwtToken = helpers.issueJWT(user);

            var params = {
                "token": jwtToken,
                "email": user.email,
                "username": user.username,
                "type": user.type,
                "_id": user._id,
                "firstname": user.firstname,
                "lastname": user.lastname,
                "country": user.country
            }

            res.status(200).send({
                "status": "ok",
                "params": params,
                "message": "Authentication Successfully - Token Issued"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "message": "Your account is temporarily deactivated.Please contact support to activate again."
            });
        }

    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Authentication Failed - Credentials Mismatch"
        });
    }
});

router.post('/signup', async function(req, res, next) {
    var creds = req.body;
    creds['type'] = 'user'
    creds['description'] = ''
    creds['image'] = ''
    creds['status'] = true
    delete creds.confirmpassword;
    db.collection('users').find({
        'email': creds.email
    }).toArray(function(er, us) {
        if (us.length == 0) {
            db.collection('users').find({
                'username': creds.username
            }).toArray(function(er, us) {
                if (us.length == 0) {
                    db.collection('users').insert(creds, function(er, stat) {
                        if (er) {
                            res.status(400).send({
                                "status": "error",
                                "message": "Something bad happened.Please try later."
                            });
                        } else {
                            transporter.sendMail({
                                from: "dotnet.rev@gmail.com",
                                to: creds.email,
                                // to: 'aseth200@gmail.com',
                                subject: 'Support-ZOHO',
                                html: `
                            <html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<style type="text/css">

	/* GENERAL EMAIL CLIENT FIXES */
	#outlook a{padding:0;}
	.ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
	.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
	body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}
	table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
	table{border-collapse:collapse !important; table-layout:fixed; !important; margin: 0 auto; !important;}
	table table {table-layout: auto;}
	img{-ms-interpolation-mode:bicubic;}
	body{margin:0; padding:0;}
	img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}

	/* NO MORE BLUE LINKS ON IPHONE */
	.appleLinks a{color: #4B4C4C;text-decoration: none;}
	.appleLinksWhite a{color: #FFFFFF;text-decoration: none;}

	/* MEDIA QUERY SCREEN 414px */
	@-ms-viewport{width:device-width}
	@media only screen and (max-width:414px){
		table[class="container"]{width:100% !important;}
		td[class="text-center"]{text-align: center !important; width:100% !important;}
		td[class="hide"]{display: none !important;}
		table[class="button-width"]{width:100% !important;}
		img[class="crop"]{height:auto !important; max-width:600px !important; width:100% !important;}
	}
	@media screen and (min-width: 601px) {
		table[class="containerDesktop"]{width: 600px!important;}
	}

	@media (min-width: 400px) and (max-width: 418px){
		.innertablesec{
			width: 150%;
		}
	}

</style>
<title>
</title>
</head>
<body>

	<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td style="text-align: center; font-size: 0px; vertical-align: top;">
					<table align="center" border="0" cellspacing="0" cellpadding="0">
						<tbody>
							<tr>
								<td align="center" valign="middle">
									<a>
										<img src="https://s3.amazonaws.com/demodumps/18342a1a-badd-49d2-9750-f552035094de.png" style="width: 100%;">
									</a>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>

	<!-- ADDITIONAL RESOURCES STARTS HERE -->
	<table width="100%" align="center" bgcolor="#ffffff" border="0" cellspacing="0" cellpadding="0" class="innertablesec">
		<tbody>
			<tr>
				<td align="left" valign="top">
	                <table width="100%" align="center" class="containerDesktop" style="max-width: 100%;background: #fff;height:60vh;" border="0" cellspacing="0" cellpadding="0">
	                	<tbody><br>
		            		<tr>
		            			<td align="left" valign="top" style="padding: 0px 20px 20px; color: rgb(75, 76, 76); line-height: 22px !important; font-family: Arial, Helvetica, sans-serif; font-size: 16px;">
		            				<div class="mktEditable" id="Paragraph">
		            					<table width="560" align="center" class="container" border="0" cellspacing="0" cellpadding="0">
		            						<tbody>
	            								<tr>
	            									<td align="left" valign="top" style="color: rgb(75, 76, 76); line-height: 22px; font-family: Arial, Helvetica, sans-serif; font-size: 16px;padding-top: 10%;">
	            									<strong style="font-size: 24px;">Dear ` + creds.username + `,</strong>
	            									<br>
	            									<p style="font-size: 30px;line-height: 1.5;">Welcome to Ethereum Hiring! Thank you so much for joining us. You’re on your way to super-productivity and beyond!</p>

	            									</td>
	            								</tr>
	            							</tbody>
	            						</table>
	            					</div>
	            				</td>
	            			</tr>
							<tr>
								<table align="center" valign="middle" border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td align="center" valign="top" style="font-family: Arial, Helvetica, sans-serif;">
												<div class="mktEditable" id="Footer">
													<table width="100%" align="center" bgcolor="#000" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr align="center" bgcolor="#000">
																<td><br>
																	<p style="color: #fff;">© Ethereum Hiring 2018. All rights reserved. Terms &amp; condition</p>
																	<hr style="width: 50%;">
																</td>
															</tr>
															<tr>
																<td align="center" bgcolor="#000">
																	<table width="600" align="center" class="container" border="0" cellspacing="0" cellpadding="0">
																	<tbody>
																		<tr>
																			<td align="center" valign="middle" style="padding: 16px 20px 10px;">
																				<table align="center" border="0" cellspacing="0" cellpadding="0">
																					<tbody style="text-align: center;">
																					</tbody>
																				</table>
																			</td>
																		</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</tr>
	        			</tbody>
	        		</table>
				</td>
			</tr>
		</tbody>
	</table>


</body>
</html>
                            `
                            }, function(error, response) {
                                if (error) {
                                    res.status(400).send(error);
                                } else {
                                    res.status(200).send({
                                        "status": "ok",
                                        "message": "User registered successfully."
                                    });
                                }
                            });

                        }
                    })
                } else {
                    res.status(400).send({
                        "status": "error",
                        "message": "Username already exists"
                    });
                }
            })
        } else {
            res.status(400).send({
                "status": "error",
                "message": "E-mail already exists"
            });
        }
    })
})
router.post('/change_password', async function(req, res, next) {
    var creds = req.body;
    db.collection('users').find({
        'email': creds.email
    }).toArray(function(er, us) {
        if (us.length == 0) {
            res.status(400).send({
                "status": "error",
                "message": "No user found for that ID"
            });
        } else {
            if (us[0].password == creds.password) {
                db.collection('users').update({
                    'email': creds.email
                }, {
                    $set: {
                        'password': creds.new_password
                    }
                }, function(eee, u) {
                    if (eee) {
                        res.status(400).send({
                            "status": "error",
                            "message": "Something bad happened.Please try later."
                        });
                    } else {
                        res.status(200).send({
                            "status": "ok",
                            "message": "Password changed successfully."
                        });
                    }
                })
            } else {
                res.status(400).send({
                    "status": "error",
                    "message": "Verification failed.Please input correct password."
                });
            }
        }
    })
})
router.post('/getJobByUserId', async function(req, res, next) {
    // var creds = req.body;
    // db.collection('appliedJobs').find({
    //     'userId': creds.userId
    // }).toArray(function (e, u) {
    //     if (u.length == 0) {
    //         res.status(400).send({
    //             "status": "error",
    //             "message": "You haven't applied any jobs yet"
    //         });
    //     } else {
    //         res.status(200).send({
    //             "status": "ok",
    //             "params": u
    //         });
    //     }
    // })
    var query = {};
    if (req.body.search_title) {
        var regex1 = new RegExp(req.body.search_title, "i")
        query = {
            'userId': req.body.userId,
            $or: [{
                'title': { $regex: regex1, $options: 'i' }
            }]
        }
    } else {
        query = {
            'userId': req.body.userId
        }
    }
    var records = await db.collection('appliedJobs').find(query).toArray();
    if (records.length > 0) {
        res.status(200).send({
            "status": "ok",
            "params": records
        });
    } else {
        res.status(400).send({
            "status": "error",
            "message": "No Jobs found !!"
        });
    }
})
router.post('/reset_password_with_otp', async function(req, res, next) {
    var creds = req.body;
    var otp1 = creds.otp.split('@');
    var otp = otp1[0];
    var id = otp1[1];
    db.collection('users').find({
        '_id': ObjectId(id)
    }).toArray(function(er, us) {
        if (us.length == 0) {
            res.status(400).send({
                "status": "error",
                "message": "No user found for that mail ID"
            });
        } else {
            if (us[0].otp == otp) {
                db.collection('users').update({
                    '_id': ObjectId(id)
                }, {
                    $set: {
                        'password': creds.new_password
                    },
                    $unset: {
                        otp: 1
                    }
                }, function(er, u) {
                    if (er) {
                        res.status(400).send({
                            "status": "error",
                            "message": "Something bad happened.Please try later."
                        });
                    } else {
                        res.status(200).send({
                            "status": "ok",
                            "message": "Password reset successfully."
                        });
                    }
                })
            } else {
                res.status(400).send({
                    "status": "error",
                    "message": "Invalid OTP entered.Please try again."
                });
            }
        }
    })
})


router.post('/change_access', async function(req, res, next) {
    var creds = req.body;
    db.collection('users').find({
        '_id': ObjectId(creds._id)
    }).toArray(function(er, user) {
        if (user[0].type == 'Administrator') {
            var type = user[0].type;
            if (type == 'Administrator') {
                type = 'user'
            } else {
                type = 'Administrator';
            }
            db.collection('users').update({
                '_id': ObjectId(creds._id)
            }, {
                $set: {
                    'type': type
                }
            }, function(eee, u) {
                if (eee) {
                    res.status(400).send({
                        "status": "error",
                        "message": "Something bad happened.Please try later."
                    });
                } else {
                    res.status(200).send({
                        "status": "ok",
                        "message": "User privileges changed successfully."
                    });
                }
            })
        } else {
            res.status(400).send({
                "status": "error",
                "message": "You need admin privileges to perform this action."
            });
        }
    })
})
router.post('/change_activation_status', async function(req, res, next) {
    var creds = req.body;
    db.collection('users').find({
        '_id': ObjectId(creds._id)
    }).toArray(function(er, user) {
        if (user[0].type == 'Administrator') {
            var type = user[0].status;
            if (type == true) {
                type = false
            } else {
                type = true
            }
            db.collection('users').update({
                '_id': ObjectId(creds._id)
            }, {
                $set: {
                    'status': type
                }
            }, function(eee, u) {
                if (eee) {
                    res.status(400).send({
                        "status": "error",
                        "message": "Something bad happened.Please try later."
                    });
                } else {
                    res.status(200).send({
                        "status": "ok",
                        "message": "User privileges changed successfully."
                    });
                }
            })
        } else {
            res.status(400).send({
                "status": "error",
                "message": "You need admin privileges to perform this action."
            });
        }
    })
})
router.post('/reset_password', async function(req, res, next) {
    var creds = req.body;
    db.collection('users').find({
        'email': creds.email
    }).toArray(function(e, user) {
        if (user.length == 0) {
            res.status(400).send({
                "status": "error",
                "message": "No user found for that mail ID"
            });
        } else {
            var otp = helpers.makeId();
            db.collection('users').update({
                'email': creds.email
            }, {
                $set: {
                    'otp': otp
                }
            }, function(er, u) {
                if (er) {
                    res.status(400).send({
                        "status": "error",
                        "message": "Something bad happened.Please try later."
                    });
                } else {
                    transporter.sendMail({
                        from: "dotnet.rev@gmail.com",
                        to: creds.email,
                        // to: 'aseth200@gmail.com',
                        subject: 'Support-ZOHO',
                        html: `
                <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<style type="text/css">

/* GENERAL EMAIL CLIENT FIXES */
#outlook a{padding:0;}
.ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}
table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
table{border-collapse:collapse !important; table-layout:fixed; !important; margin: 0 auto; !important;}
table table {table-layout: auto;}
img{-ms-interpolation-mode:bicubic;}
body{margin:0; padding:0;}
img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}

/* NO MORE BLUE LINKS ON IPHONE */
.appleLinks a{color: #4B4C4C;text-decoration: none;}
.appleLinksWhite a{color: #FFFFFF;text-decoration: none;}

/* MEDIA QUERY SCREEN 414px */
@-ms-viewport{width:device-width}
@media only screen and (max-width:414px){
table[class="container"]{width:100% !important;}
td[class="text-center"]{text-align: center !important; width:100% !important;}
td[class="hide"]{display: none !important;}
table[class="button-width"]{width:100% !important;}
img[class="crop"]{height:auto !important; max-width:600px !important; width:100% !important;}
}
@media screen and (min-width: 601px) {
table[class="containerDesktop"]{width: 600px!important;}
}

@media (min-width: 400px) and (max-width: 418px){
.innertablesec{
width: 150%;
}
}

</style>
<title>
</title>
</head>
<body>

<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="0">
<tbody>
<tr>
    <td style="text-align: center; font-size: 0px; vertical-align: top;">
        <table align="center" border="0" cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <td align="center" valign="middle">
                        <a>
                            <img src="https://s3.amazonaws.com/demodumps/18342a1a-badd-49d2-9750-f552035094de.png" style="width: 100%;">
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>

<!-- ADDITIONAL RESOURCES STARTS HERE -->
<table width="100%" align="center" bgcolor="#ffffff" border="0" cellspacing="0" cellpadding="0" class="innertablesec">
<tbody>
<tr>
    <td align="left" valign="top">
        <table width="100%" align="center" class="containerDesktop" style="max-width: 100%;background: #fff;height:60vh;" border="0" cellspacing="0" cellpadding="0">
            <tbody><br>
                <tr>
                    <td align="left" valign="top" style="padding: 0px 20px 20px; color: rgb(75, 76, 76); line-height: 22px !important; font-family: Arial, Helvetica, sans-serif; font-size: 16px;">
                        <div class="mktEditable" id="Paragraph">
                            <table width="560" align="center" class="container" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td align="left" valign="top" style="color: rgb(75, 76, 76); line-height: 22px; font-family: Arial, Helvetica, sans-serif; font-size: 16px;padding-top: 10%;">
                                        <strong style="font-size: 24px;">Dear ` + user[0].username + `,</strong>
                                        <br>
                                        <p style="font-size: 30px;line-height: 1.5;">Please click this link to reset your password - </p>http://hiring.eprofitanalyzer.com/reset-password/` + otp + '@' + user[0]._id + `

                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <table align="center" valign="middle" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td align="center" valign="top" style="font-family: Arial, Helvetica, sans-serif;">
                                    <div class="mktEditable" id="Footer">
                                        <table width="100%" align="center" bgcolor="#000" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr align="center" bgcolor="#000">
                                                    <td><br>
                                                        <p style="color: #fff;">© Ethereum Hiring 2018. All rights reserved. Terms &amp; condition</p>
                                                        <hr style="width: 50%;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" bgcolor="#000">
                                                        <table width="600" align="center" class="container" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" valign="middle" style="padding: 16px 20px 10px;">
                                                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody style="text-align: center;">
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </tr>
            </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>


</body>
</html>
                `
                    }, function(error, response) {
                        if (error) {
                            res.status(400).send(error);
                        } else {
                            res.status(200).send({
                                "status": "ok",
                                "message": "E-mail has been sent to your registered mail ID."
                            });
                        }
                    });
                }
            })
        }
    })

})
router.get('/countryList', async function(req, res, next) {
    db.collection('country').find({}).toArray(function(er, country) {
        if (er) {
            res.status(400).send({
                "status": "error",
                "message": "Something bad happened.Please try later."
            });
        } else {
            res.status(200).send({
                "status": "ok",
                "params": country,
            });
        }
    })
})

router.get('/user/profile', async function(req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var user = await db.collection('users').findOne({
            username: req.query['username'],
            email: req.query['email']
        });
        if (user) {
            res.status(200).send({
                "status": "ok",
                "params": user,
                "message": "User Profile Fetched Successfully"
            });
        } else {
            res.status(200).send({
                "status": "error",
                "params": "",
                "message": "User not Found"
            });
        }

    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});
router.post('/user/profile/update', async function(req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var check_user = await db.collection('users').find({ "email": tokenData.email }).toArray();
        var urlResume1 = check_user[0].resume1;
        var urlResume2 = check_user[0].resume2;
        if (req.body.resume1_ext) {

            req.body.resume1 = await exports.uploadDoc(req.body.resume1, req.body.resume1_ext, req.body.username);
        } else {
            if (urlResume1) {
                req.body.resume1 = urlResume1;
            } else {
                req.body.resume1 = "";
            }

        }

        if (req.body.resume2_ext) {
            req.body.resume2 = await exports.uploadDoc(req.body.resume2, req.body.resume2_ext, req.body.username);
        } else {
            if (urlResume2) {
                req.body.resume2 = urlResume2;
            } else {
                req.body.resume2 = "";
            }
        }

        var user = await db.collection('users').update({
            '_id': ObjectId(req.body._id)
        }, {
            $set: {
                'firstname': req.body.firstname,
                'lastname': req.body.lastname,
                'address': req.body.address.trim(),
                'city': req.body.city,
                'country': req.body.country,
                'zipcode': req.body.zipcode,
                'description': req.body.description,
                "image": req.body.image,
                "nationality": req.body.nationality.trim(),
                "english": req.body.english,
                "japanese": req.body.japanese,
                "phone":req.body.phone,
                "visa": req.body.visa,
                "reach_us": req.body.reach_us,
                "join_in_weeks": req.body.join_in_weeks,
                "reason_to_leave": req.body.reason_to_leave,
                "current_salary": req.body.current_salary,
                "expected_salary": req.body.expected_salary,
                "resume1": req.body.resume1,
                "resume2": req.body.resume2
            }
        });
        if (user) {
            res.status(200).send({
                "status": "ok",
                "params": "",
                "message": "Profile Successfully Updated"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Profile could not be updated"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

// Jobs

router.get('/jobs/all', async function(req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var totalJobs = await db.collection('jobs').count();
        if (req.query['number']) {
            let paginate = (req.query['number'] - 1) * 12; //skip number of entry
            var jobs = await db.collection('jobs').find({}).skip(paginate).limit(12).toArray();
        } else {
            var jobs = await db.collection('jobs').find({}).limit(12).toArray();
        }

        if (jobs) {
            res.status(200).send({
                "status": "ok",
                "params": {
                    jobs: jobs,
                    totalJobs: totalJobs
                },
                "message": "Jobs Fetched Successfully"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Jobs within the specified range not found"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

// Cashback

router.post('/cashback/create', async function(req, res, next) {
    var cashback = await db.collection('cashback').insert(req.body);

    if (cashback) {
        res.status(200).send({
            "status": "ok",
            "params": "",
            "message": "Cashback Successfully Added"
        });
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Cashback could not be added"
        });
    }
});

router.get('/cashback/view', async function(req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var totalCash = await db.collection('cashback').count();
        if (req.query['number']) {
            let paginate = (req.query['number'] - 1) * 10; //skip number of entry
            var cash = await db.collection('cashback').find({}).skip(paginate).limit(10).toArray();
        } else {
            var cash = await db.collection('cashback').find({}).limit(10).toArray();
        }

        if (cash) {
            res.status(200).send({
                "status": "ok",
                "params": {
                    cash: cash,
                    totalCash: totalCash
                },
                "message": "Cashback Fetched Successfully"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Cashback could not be fetched"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

router.post('/cashback/delete', async function(req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var cashback = await db.collection('cashback').deleteOne({
            '_id': ObjectId(req.body.id)
        });
        if (cashback) {
            res.status(200).send({
                "status": "ok",
                "params": "",
                "message": "Cashback Successfully Deleted"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Cashback could not be deleted"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

router.post('/cashback/update', async function(req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var cashback = await db.collection('cashback').update({
            '_id': ObjectId(req.body._id)
        }, {
            $set: {
                'sector': req.body.sector,
                'position': req.body.position,
                'amount': req.body.amount
            }
        });

        if (cashback) {
            res.status(200).send({
                "status": "ok",
                "params": "",
                "message": "Cashback Successfully Updated"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Cashback could not be updated"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

//  _____         __             __  __            __
// /__  /  ____  / /_  ____     / / / /___  ____  / /_______
//   / /  / __ \/ __ \/ __ \   / /_/ / __ \/ __ \/ //_/ ___/
//  / /__/ /_/ / / / / /_/ /  / __  / /_/ / /_/ / ,< (__  )
// /____/\____/_/ /_/\____/  /_/ /_/\____/\____/_/|_/____/
//

router.post('/zoho/jobs/all', async function(req, res, next) {
    console.log("coming in this api")
        // try {
    var counter = 0;
    var fromIndex = 1;
    var toIndex = 200;

    // while (counter != 1) {
    var response = await axios.get(process.env.ZOHO_URL + '/JobOpenings/getRecords', {
        params: {
            authtoken: process.env.ZOHO_TOKEN,
            scope: process.env.ZOHO_SCOPE,
            fromIndex: fromIndex,
            toIndex: toIndex
        }
    });


    var keyConditions = ["Job Opening ID", "Account Manager", "Job Type", "Is Hot Job Opening", "Publish", "No of Candidates Associated", "Posting Title", "Job Opening Status", "Date Opened", "Target Date", "Client Name", "Contact Name", "City", "State", "Country", "Industry", "Number of Positions", "Created By", "Work Experience", "Salary", "Job Description", "fee", "Required Japanese level", "Zip Code"];

    if (response.data.response.result) {
        var jobs = response.data.response.result.JobOpenings.row;
        var arr = [];
        // res.status(200).send({
        //     "status": "ok",
        //     "params": jobs,
        //     "message": "Job Listing Retrived"
        // });
        for (var i = 0; i < jobs.length; i++) {
            var ob = {}
            console.log(jobs[i]['FL']);
            for (var j = 0; j < jobs[i]['FL'].length; j++) {
                // console.log(jobs[i].FL[j].content)
                if (keyConditions.indexOf(jobs[i].FL[j].val) != -1)
                    ob[jobs[i].FL[j].val] = jobs[i].FL[j].content;
            }
            arr.push(ob);
        }
        var start_sal = 0;
        var end_sal;
        var start_workex = 0;
        var end_workex;
        for (var j = 0; j < arr.length; j++) {
            if (arr[j]["Work Experience"]) {
                var num = arr[j]["Work Experience"].split(" ");
                var s = num[0].split('-');
                start_workex = parseInt(s[0]);
                end_workex = parseInt(s[1]);
            }
            if (arr[j]['Salary']) {
                var ss = arr[j]['Salary'].split('~');
                start_sal = parseInt(ss[0]);
                end_sal = parseInt(ss[1]);
            }
            arr[j].start_sal = start_sal;
            arr[j].end_sal = end_sal;
            arr[j].start_workex = start_workex;
            arr[j].end_workex = end_workex;
        }
        await db.collection('jobs').insert(arr, function(eee, j) {
            if (eee) {
                res.status(400).send({
                    "status": "error",
                    "message": "Something bad happened.Please try later."
                });
            } else {
                console.log("coming in else")
                res.status(200).send({
                    "status": "ok",
                    "params": arr,
                    "message": "Job Listing Retrived"
                });
            }
        });

        // for (i = 0; i < jobs.length; i++) {
        //     var details = [];
        //     for (var j = 0; j < keyConditions.length; j++) {
        //         var detailIndex = _.findIndex(jobs[i].FL, {
        //             'val': keyConditions[j],
        //         });

        //         details.push({
        //             [keyConditions[j]]: jobs[i].FL[detailIndex].content
        //         });
        //     }

        //     console.log(_.defaults(details));

        //     // await db.collection('jobs').insert(_.fromPairs(details));
        // }

    } else {
        counter = 1;
    }

    // fromIndex = toIndex;
    // toIndex = toIndex + 200;
    // }

    // } catch (error) {
    //     res.status(400).send({
    //         "status": "error",
    //         "params": "",
    //         "message": "Zoho Job Listing could not pulled : " + error
    //     });
    // }
});

router.post('/zoho/jobs/byFilter', async function(req, res, next) {
    console.log("online 9781");
    var creds = req.body;
    var query = {};
    var totalJobs = await db.collection('jobs').count();
    if (creds.work_experience) {
        query = {
            'start_workex': {
                $lte: parseInt(creds.work_experience)
            },
            'end_workex': {
                $gte: parseInt(creds.work_experience)
            }
        }
    }
    if (creds.salary) {
        query = {
            $and: [{
                'start_sal': {
                    $lte: parseInt(creds.salary)
                }
            }, {
                $or: [{
                    'end_sal': {
                        $gte: parseInt(creds.salary)
                    }
                }, {
                    'end_sal': null
                }]
            }]
        }
    }
    if (creds.job_type) {
        query = {
            'Job Type': creds.job_type
        }
    }
    var paginate = 0;
    if (creds['number']) {
        paginate = (creds['number'] - 1) * 10; //skip number of entry
    }
    var totalJobs = await db.collection('jobs').find(query).count();
    db.collection('jobs').find(query).skip(paginate).limit(12).toArray(function(er, jobs) {
        if (er) {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Something bad happened.Please try later"
            });
        } else {
            res.status(200).send({
                "status": "ok",
                "params": {
                    'jobs': jobs,
                    'totalJobs': totalJobs
                },
            });
        }
    })
});


router.post('/zoho/jobs/getBySearchCashBack', async function(req, res, next) {
    var creds = req.body;
    var query = {};
    if (creds.search_key) {
        console.log("this", creds.search_key);
        var cashBack = creds.search_key;
        var converted = '';
        if (cashBack.indexOf("~") > 0) {
            converted = cashBack.replace('-', '~');
        } else {
            converted = cashBack;
        }
        console.log(converted);
        var regex1 = new RegExp(creds.search_key, "i")
        var fields = converted.split('~');
        var startCashBAck = fields[0];
        var endCashBack = fields[1];
        if (converted.indexOf("~") > 0) {
            query = {
                $and: [{ "start_cashback": { "$gte": parseInt(startCashBAck), "$lte": parseInt(endCashBack) } },
                    {
                        $or: [{
                                "end_cashback": { "$gte": parseInt(endCashBack) }
                            }, {
                                'Required Japanese level': { $regex: regex1, $options: 'i' }
                            }, {
                                'Date Opened': { $regex: regex1, $options: 'i' }
                            }, {
                                'Posting Title': { $regex: regex1, $options: 'i' }
                            }, {
                                'City': { $regex: regex1, $options: 'i' }
                            }, {
                                'Job Opening ID': { $regex: regex1, $options: 'i' }
                            }, {
                                'Job Type': { $regex: regex1, $options: 'i' }
                            },
                            {
                                /*'Salary': { $regex: regex1, $options: 'i' }*/
                                "start_sal": { "$gte": parseInt(startCashBAck), "$lte": parseInt(endCashBack) }
                            }, {
                                'end_sal': {
                                    $gte: parseInt(endCashBack)
                                }
                            }
                        ]
                    }
                ]
            }
            console.log(query, "1");
        } else {
            var regex1 = new RegExp(creds.search_key, "i")
            query = {
                $or: [{
                        'Required Japanese level': { $regex: regex1, $options: 'i' }
                    }, {
                        'Date Opened': { $regex: regex1, $options: 'i' }
                    }, {
                        'Posting Title': { $regex: regex1, $options: 'i' }
                    }, {
                        'City': { $regex: regex1, $options: 'i' }
                    }, {
                        'Job Opening ID': { $regex: regex1, $options: 'i' }
                    }, {
                        'Job Type': { $regex: regex1, $options: 'i' }
                    }, {
                        'Salary': { $regex: regex1, $options: 'i' }
                    },
                    {
                        'cashback': { $regex: regex1, $options: 'i' }
                    }
                ]
            }
            console.log(query, "2");
        }

        var paginate = 0;
        if (creds['number']) {
            paginate = (creds['number'] - 1) * 10; //skip number of entry
        }
        var totalJobs = await db.collection('jobs').find(query).count();

        console.log(util.inspect(query, false, null));

        db.collection('jobs').find(query).skip(paginate).limit(12).toArray(function(er, jobs) {
            if (er) {
                res.status(400).send({
                    "status": "error",
                    "params": "",
                    "message": "Something bad happened.Please try later"
                });
            } else {
                res.status(200).send({
                    "status": "ok",
                    "params": { 'jobs': jobs, 'totalJobs': totalJobs }
                });
            }
        })
    } else {
        var paginate = 0;
        if (creds['number']) {
            paginate = (creds['number'] - 1) * 10; //skip number of entry
        }
        var totalJobs = await db.collection('jobs').find(query).count();

        console.log(util.inspect(query, false, null));

        db.collection('jobs').find(query).skip(paginate).limit(12).toArray(function(er, jobs) {
            if (er) {
                res.status(400).send({
                    "status": "error",
                    "params": "",
                    "message": "Something bad happened.Please try later"
                });
            } else {
                res.status(200).send({
                    "status": "ok",
                    "params": { 'jobs': jobs, 'totalJobs': totalJobs }
                });
            }
        })
    }
});


// salary  search filter api working
router.post('/zoho/jobs/salary', async function(req, res, next) {
    var creds = req.body;
    var query = {};
    if (creds.search_key) {
        var fields = creds.search_key.split('~');
        var start = fields[0];
        var end = fields[1];
        if (creds.search_key) {
            var regex1 = new RegExp(creds.search_key, "i");
            if (creds.search_key.search('~') > 0) {
                query = {
                    // $or: [{
                    //     'start_sal': { '$gte': parseInt(start) },
                    //     'end_sal': { '$lte': parseInt(end) },
                    //     'start_sal': { '$lte': parseInt(end) },
                    //     'end_sal': { '$gte': parseInt(start) }
                    // }]
                    'start_sal': { '$gte': parseInt(start) },
                    'end_sal': { '$lte': parseInt(end) }
                };
            }
        }
    }
    var paginate = 0;
    if (creds['number']) {
        paginate = (creds['number'] - 1) * 10; //skip number of entry
    }
    var totalJobs = await db.collection('jobs').find(query).count();
    db.collection('jobs').find(query).skip(paginate).limit(12).toArray(function(er, jobs) {
        if (er) {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Something bad happened.Please try later"
            });
        } else {
            res.status(200).send({
                "status": "ok",
                "params": {
                    'jobs': jobs,
                    'totalJobs': totalJobs
                },
            });
        }
    })
});


router.post('/zoho/jobs/getBySearch', async function(req, res, next) {
        var creds = req.body;
        var query = {};
        if (creds.search_key) {
            var regex1 = new RegExp(creds.search_key, "i")
            query = {
                $or: [{
                    'Required Japanese level': { $regex: regex1, $options: 'i' }
                }, {
                    'Date Opened': { $regex: regex1, $options: 'i' }
                }, {
                    'Posting Title': { $regex: regex1, $options: 'i' }
                }, {
                    'City': { $regex: regex1, $options: 'i' }
                }, {
                    'Job Opening ID': { $regex: regex1, $options: 'i' }
                }, {
                    'Job Type': { $regex: regex1, $options: 'i' }
                }]
            }
        }

        var paginate = 0;
        if (creds['number']) {
            paginate = (creds['number'] - 1) * 10; //skip number of entry
        }
        var totalJobs = await db.collection('jobs').find(query).count();

        console.log(util.inspect(query, false, null));

        db.collection('jobs').find(query).skip(paginate).limit(12).toArray(function(er, jobs) {
            if (er) {
                res.status(400).send({
                    "status": "error",
                    "params": "",
                    "message": "Something bad happened.Please try later"
                });
            } else {
                res.status(200).send({
                    "status": "ok",
                    "params": { 'jobs': jobs, 'totalJobs': totalJobs }
                });
            }
        })
    })
    /*  cashback query  **/
router.post('/zoho/jobs/getJobByCashback', async function(req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var creds = req.body;
        var query = {};
        var fields = creds.search_key.split('~');
        var start = fields[0];
        var end = fields[1];
        if (creds.search_key) {
            var regex1 = new RegExp(creds.search_key, "i");
            if (creds.search_key.search('~') > 0) {
                // query = {
                //     $or: [{
                //         'start_cashback': { '$gte': parseInt(start) },
                //         'end_cashback': { '$lte': parseInt(end) },
                //         'start_cashback': { '$lte': parseInt(end) },
                //         'end_cashback': { '$gte': parseInt(start) }
                //     }]
                // };
                query = {
                    "start_cashback": { "$gte": parseInt(start) },
                    "end_cashback": { "$lte": parseInt(end) }
                };
            }
        }

        var paginate = 0;
        if (creds['number']) {
            paginate = (creds['number'] - 1) * 10; //skip number of entry
        }
        var totalJobs = await db.collection('jobs').find(query).count();

        var jobs = await db.collection('jobs').find(query).skip(paginate).limit(12).toArray();
        console.log('jobs', jobs);
        if (jobs) {
            res.status(200).send({
                "status": "ok",
                "params": {
                    'jobs': jobs,
                    'totalJobs': totalJobs
                },
                "message": "jobs fetched successfully"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Something bad happened.Please try later"
            });
        }

    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }

});
/**
 *
 *
 * search filter for searching users
 *
 *
 *
 */

router.post('/userSearch', async function(req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var creds = req.body;
        var query = {};
        if (creds.search_user) {
            var regex1 = new RegExp(creds.search_user, "i")
            query = {
                $or: [{
                    'username': { $regex: regex1, $options: 'i' }
                }, {
                    'firstname': { $regex: regex1, $options: 'i' }
                }, {
                    'lastname': { $regex: regex1, $options: 'i' }
                }, {
                    'email': { $regex: regex1, $options: 'i' }
                }]
            }
        }
        var totalUsers = await db.collection('users').find(query).count();
        db.collection('users').find(query).toArray(function(er, users) {
            if (er) {
                res.status(400).send({
                    "status": "error",
                    "params": "",
                    "message": "Something bad happened.Please try later"
                });
            } else {
                res.status(200).send({
                    "status": "ok",
                    "params": { 'users': users, 'totalUsers': totalUsers }
                });
            }
        })
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});



router.get('/getUserAppliedJobs', async function(req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var query = {};
        var jobTitle = '';
        if (req.query['search_key'] !== 'undefined' && req.query['search_key'] !== '') {
            var regex1 = new RegExp(req.query['search_key'], "i")
            jobTitle = await db.collection('appliedJobs').aggregate(
                [{
                        $match: {
                            $or: [{
                                'title': { $regex: regex1, $options: 'i' }
                            }]
                        }
                    },
                    {
                        $group: {
                            _id: "$jobId",
                            title: { $push: "$title" }
                        }
                    }
                ]).toArray();
        } else {
            jobTitle = await db.collection('appliedJobs').aggregate(
                [{
                    $group: {
                        _id: "$jobId",
                        title: { $push: "$title" }
                    }
                }]).toArray();
        }

        if (jobTitle) {
            res.status(200).send({
                "status": "ok",
                "params": jobTitle,
                "message": "Titles got Successfully"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "error got"
            });
        }

    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }

});


/*router.get('/user/getCandidates', async function(req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    //makes filter option
   // var regex = new RegExp(req.query['search_key'], "i")
    if (req.query['filter_by'] == '') {
        var filter = { $ne: 'rejected' };
    } else if (req.query['filter_by'] == 'accept') {
        var filter = { $eq: 'accept' };
    } else if (req.query['filter_by'] == 'rejected') {
        var filter = { $eq: 'rejected' };
    } else {
        var filter = { $eq: 'interview' };
    }

    if (tokenData) {
        var jobDetail = await db.collection('appliedJobs').findOne({ 'jobId': req.query['jobId'] });
        var userDetails = await db.collection('appliedJobs').find({
            'jobId': req.query['jobId'],
            'progress': filter,
            $or: [
                { 'userDetail.firstname': { $regex: req.query['search_key'], $options: 'i' } },
                { 'userDetail.lastname': { $regex: req.query['search_key'], $options: 'i' } },
                { 'userDetail.email': { $regex: req.query['search_key'], $options: 'i' } },
                { 'userDetail.nationality': { $regex: req.query['search_key'], $options: 'i' } },
            ]
        }).project({ userDetail: 1, _id: 1, status: 1, interested_in_company: 1, interested_in_position:1 }).toArray();
        console.log(userDetails);
        if (userDetails) {
            res.status(200).send({
                "status": "ok",
                "params": { 'jobDetail': jobDetail.jobDetail, 'userDetails': userDetails },
                "message": "Candidates list fetched successfully"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Candidates list could not be fetched"
            });
        }

    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }

});*/

router.get('/user/getCandidate', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var jobDetail = await db.collection('appliedJobs').findOne({ 'jobId': req.query['jobId'] });
        if (!jobDetail) {
            jobDetail = await db.collection('jobs').findOne({ '_id': ObjectId(req.query['jobId']) });
        } else {
            jobDetail = jobDetail.jobDetail;
        }
        var regex1 = new RegExp(req.query['search_key'], "i")
        var userDetails = await db.collection('appliedJobs').aggregate([
            {
                $match: {
                    "jobId": req.query['jobId'],
                    $or: [
                        {
                            'username': { $regex: regex1, $options: 'i' }
                        },
                    {
                        'firstname': { $regex: regex1, $options: 'i' }
                    }, {
                        'lastname': { $regex: regex1, $options: 'i' }
                    }, {
                        'userEmail': { $regex: regex1, $options: 'i' }
                    }] } },
            {
                "$lookup": {
                    "from": "users",
                    "localField": "userEmail",
                    "foreignField": "email",
                    "as": "userDetails"
                }
            }, {
                "$lookup": {
                    "from": "wallets",
                    "localField": "userEmail",
                    "foreignField": "email",
                    "as": "wallet"
                }
            }
            , {
                $project: {
                    interested_in_company: 1,
                    interested_in_position: 1,
                    userDetails: 1,
                    userId: 1, status: 1,initiate:1, "wallet.address": 1
                }
            }
        ]).toArray();
        if (jobDetail) {
            res.status(200).send({
                "status": "ok",
                "params": { 'jobDetail': jobDetail, 'userDetails': userDetails },
                "message": "Candidates list fetched successfully"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Candidates list could not be fetched"
            });
        }

    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

exports.uploadDoc = async function (document, extension,name) {
    try {
        var Eresume = document;
        let buff = await new Buffer(Eresume.toString('utf8'), 'base64');
        var filename = await Date.now() + "_" + name + "." + extension;
        var file = await fs.writeFileSync(process.env.FILEPATH + "/" + filename, buff);
        return process.env.WEBURL + "/" + filename;
    } catch (error) {
        return "Corruted Document";
    }
}
//     ______                      __     ____              __
//    / ____/  ______  ____  _____/ /_   / __ \____  __  __/ /____  _____
//   / __/ | |/_/ __ \/ __ \/ ___/ __/  / /_/ / __ \/ / / / __/ _ \/ ___/
//  / /____>  </ /_/ / /_/ / /  / /_   / _, _/ /_/ / /_/ / /_/  __/ /
// /_____/_/|_/ .___/\____/_/   \__/  /_/ |_|\____/\__,_/\__/\___/_/
//           /_/

module.exports = router;