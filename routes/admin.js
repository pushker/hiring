var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var jwt = require('jsonwebtoken');
var axios = require('axios');
var _ = require('lodash');
var helpers = require('../helpers');
var ObjectId = require('mongodb').ObjectId;
var moment = require('moment');

var db;
MongoClient.connect(process.env.MONGO_URL, function (err, client) {
    if (err) {
        throw err;
    }
    db = client.db(process.env.MONGO_DATABASE);
});

/**
 *
 *admin cashback dropdown
 */
router.post('/addCashback', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    var creds = req.body;
    if (tokenData) {
        if (creds.from && creds.to) {
            var cashback = creds.from + "~" + creds.to;
            var count = await db.collection('adminCashback').find({ "cashback": cashback }).count();
            if (count == 0) {
                var cash = await db.collection('adminCashback').insert({ "cashback": cashback, "from": parseInt(creds.from), "to": parseInt(creds.to) });
                res.status(200).send({
                    "status": "ok",
                    "params": "",
                    "message": "cashback created successfully"
                });
            } else {
                res.status(400).send({
                    "status": "ok",
                    "message": "Cashback already in the list, Please add another one"
                });
            }
        } else {
            res.status(400).send({
                "status": "error",
                "message": "empty values"
            })
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

/**
 *
 *
 * admin salary dropdown
 */
router.post('/addSalary', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    var creds = req.body;
    if (tokenData) {
        if (creds.from && creds.to) {
            var salary = creds.from + "~" + creds.to;
            var count = await db.collection('adminSalary').find({ "salary": salary }).count();
            if (count == 0) {
                var cash = await db.collection('adminSalary').insert({ "salary": salary, "from": parseInt(creds.from), "to": parseInt(creds.to) });
                res.status(200).send({
                    "status": "ok",
                    "params": "",
                    "message": "Salary created successfully"
                });
            } else {
                res.status(400).send({
                    "status": "ok",
                    "message": "Salary already in the list, Please add another one"
                });
            }
        } else {
            res.status(400).send({
                "status": "error",
                "message": "empty values"
            })
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});


router.get('/getAllRangeOptions', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var cashbacks = await db.collection('adminCashback').find({}).toArray();
        var salary = await db.collection('adminSalary').find({}).toArray();
        res.status(200).send({
            "status": "ok",
            "params": { "cashbacks": cashbacks, "salaryRange": salary },
            "message": "Cashback and Salary ranges found successfully"
        })
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

/**
 *
 * update admin cashback amount
 *
 */
router.post('/updateCashback', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var creds = req.body;
        var validId = ObjectId.isValid(creds._id);
        if (validId == true) {
            db.collection('adminCashback').find({
                '_id': ObjectId(creds._id)
            }).toArray(function (err, resp) {
                if (err) {
                    res.status(400).send({
                        "status": "error",
                        "message": "No cashback found for that ID"
                    });
                } else {
                    db.collection('adminCashback').update({
                        '_id': ObjectId(creds._id)
                    }, {
                            $set: {
                                "cashback": creds.from + "~" + creds.to,
                                "from": parseInt(creds.from),
                                "to": parseInt(creds.to)
                            }
                        }, function (error, response) {
                            if (error) {
                                res.status(400).send({
                                    "status": "error",
                                    "message": "Something bad happened.Please try later."
                                });
                            } else {
                                res.status(200).send({
                                    "status": "ok",
                                    "message": "Cashback amount changed successfully."
                                });
                            }
                        })
                }
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": ObjectId.isValid(creds._id),
                "message": "caskback of this ID is not valid ID"
            });
        }

    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});
/**
 *
 * update admin salary amount
 *
 */
router.post('/updateSalary', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var creds = req.body;
        var validId = ObjectId.isValid(creds._id);
        if (validId == true) {
            db.collection('adminSalary').find({
                '_id': ObjectId(creds._id)
            }).toArray(function (err, resp) {
                if (err) {
                    res.status(400).send({
                        "status": "error",
                        "message": "No salary found for that ID"
                    });
                } else {
                    db.collection('adminSalary').update({
                        '_id': ObjectId(creds._id)
                    }, {
                            $set: {
                                "salary": creds.from + "~" + creds.to,
                                "from": parseInt(creds.from),
                                "to": parseInt(creds.to)
                            }
                        }, function (error, response) {
                            if (error) {
                                res.status(400).send({
                                    "status": "error",
                                    "message": "Something bad happened.Please try later."
                                });
                            } else {
                                res.status(200).send({
                                    "status": "ok",
                                    "message": "Salary amount changed successfully."
                                });
                            }
                        })
                }
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": ObjectId.isValid(creds._id),
                "message": "caskback of this ID is not valid ID"
            });
        }

    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

router.post('/deleteCashback', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var creds = req.body;
        var validId = ObjectId.isValid(creds._id);
        if (validId == true) {
            db.collection('adminCashback').find({
                '_id': ObjectId(creds._id)
            }).toArray(function (err, resp) {
                if (err) {
                    res.status(400).send({
                        "status": "error",
                        "message": "No cashback found for that ID"
                    });
                } else {
                    db.collection('adminCashback').deleteOne({
                        '_id': ObjectId(creds._id)
                    }, function (error, response) {
                        if (error) {
                            res.status(400).send({
                                "status": "error",
                                "message": "Something bad happened.Please try later."
                            });
                        } else {
                            console.log(response);
                            res.status(200).send({
                                "status": "ok",
                                "params": "",
                                "message": "Cashback Successfully Deleted"
                            });
                        }
                    });
                }
            })
        } else {
            res.status(400).send({
                "status": "error",
                "params": ObjectId.isValid(creds._id),
                "message": "caskback of this ID is not valid ID"
            });
        }

    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});
/***
 *
 *
 * delete salary from admin salary
 *
 *
 */
router.post('/deleteSalary', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var creds = req.body;
        var validId = ObjectId.isValid(creds._id);
        if (validId == true) {
            db.collection('adminSalary').find({
                '_id': ObjectId(creds._id)
            }).toArray(function (err, resp) {
                if (err) {
                    res.status(400).send({
                        "status": "error",
                        "message": "No salary found for that ID"
                    });
                } else {
                    db.collection('adminSalary').deleteOne({
                        '_id': ObjectId(creds._id)
                    }, function (error, response) {
                        if (error) {
                            res.status(400).send({
                                "status": "error",
                                "message": "Something bad happened.Please try later."
                            });
                        } else {
                            console.log(response);
                            res.status(200).send({
                                "status": "ok",
                                "params": "",
                                "message": "Salary Successfully Deleted"
                            });
                        }
                    });
                }
            })
        } else {
            res.status(400).send({
                "status": "error",
                "params": ObjectId.isValid(creds._id),
                "message": "Salary of this ID is not valid ID"
            });
        }

    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

/**
 *
 * notifications for  latest cashbacks for current to next week
 *
 */

router.get('/dash-notification-current', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var id = req.query['id'];
        var validId = ObjectId.isValid(id);

        if (validId == true) {
            //  var user = await db.collection('users').find({ '_id': req.query['id'] }).toArray();
            var user = await db.collection('users').find({
                "email": tokenData.email
            }).toArray();
            console.log(user[0].type, "dsgds");
            if (user[0].type == 'Administrator') {
                var present_date = moment().format('YYYY-MM-DD');
                var previous_week = moment().subtract(7, 'days').format('YYYY-MM-DD');
                var future_week = moment().add(7, 'days').format('YYYY-MM-DD');
                var paginate = 0;
                if (req.query['number']) {
                    paginate = (req.query['number'] - 1) * 7; //skip number of entry
                }

                var paginateComplete = 0;
                if (req.query['cashnumber']) {
                    paginateComplete = (req.query['cashnumber'] - 1) * 7; //skip number of entry
                }
                console.log(previous_week);

                var cashback = await db.collection('hiringjobs').aggregate([
                    {
                        $match: {
                            $or: [{
                                $and: [{
                                    "cashback_date1":
                                        { $gte: new Date(present_date), $lte: new Date(future_week) }
                                }, {
                                    "cashback_status1": false

                                }
                                ]
                            },
                            {
                                $and: [{
                                    "cashback_date2":
                                        { $gte: new Date(present_date), $lte: new Date(future_week) }
                                }, {
                                    "cashback_status2": false
                                }
                                ]
                            }, {
                                $and: [{
                                    "cashback_date3":
                                        { $gte: new Date(present_date), $lte: new Date(future_week) }
                                }, {
                                    "cashback_status3": false
                                }]
                            }, {
                                $and: [{
                                    "cashback_date3":
                                        { $lte: new Date(present_date)}
                                }, {
                                    "cashback_status3": false
                                    }]
                            }, {
                                $and: [{
                                    "cashback_date2":
                                        { $lte: new Date(present_date) }
                                }, {
                                    "cashback_status2": false
                                    }]
                                }, {
                                    $and: [{
                                        "cashback_date1":
                                            { $lte: new Date(present_date)}
                                    }, {
                                        "cashback_status1": false
                                    }]
                                }]
                        }
                    }, {
                        "$lookup": {
                            "from": "users",
                            "localField": "email",
                            "foreignField": "email",
                            "as": "Details"
                        }
                    },
                    { $skip: paginate }, { $limit: 7 }

                ]).toArray();

                var completeCashbacks = await db.collection('hiringjobs').aggregate([
                    {
                        $match: {
                            $or: [{
                                $or: [{
                                    "cashback_date1":
                                        { $gte: new Date(present_date), $lte: new Date(previous_week) }
                                }, {
                                    "cashback_status1": true

                                }
                                ]
                            },
                            {
                                $or: [{
                                    "cashback_date2":
                                        { $gte: new Date(present_date), $lte: new Date(previous_week) }
                                }, {
                                    "cashback_status2": true
                                }
                                ]
                            }, {
                                $or: [{
                                    "cashback_date3":
                                        { $gte: new Date(present_date), $lte: new Date(previous_week) }
                                }, {
                                    "cashback_status3": true
                                }]
                            }]
                        }
                    }, {
                        "$lookup": {
                            "from": "users",
                            "localField": "email",
                            "foreignField": "email",
                            "as": "Details"
                        }
                    },
                    { $skip: paginateComplete },
                    { $limit: 7 }
                ]).toArray();

                if (cashback || completeCashbacks) {

                    res.status(200).send({
                        "status": "ok",
                        "params": {
                            "cashback": cashback,
                            "totalCount": cashback.length,
                            "completeCashback": completeCashbacks,
                            "totalComplete": completeCashbacks.length
                        },
                        "message": "cashback details fetched successfully"
                    })

                } else {
                    res.status(400).send({
                        "status": "error",
                        "message": "No Hiring candidates avaliable"
                    });
                }

            } else {
                res.status(400).send({
                    "status": "error",
                    "params": 'no admin access',
                    "message": "Something bad happened.Please try later."
                })
            }

        } else {
            res.status(400).send({
                "status": "error",
                "message": "Something bad happened.Please try later."
            })
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

/**
 *
 * user notification
 *
 */

router.get('/user_notification', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var email = tokenData.email;
        if (email) {
            var user = await db.collection('hiringjobs').find({
                "email": email
            }).sort({ $natural: -1 }).limit(4).toArray();
            if (user) {
                res.send({
                    status: "ok",
                    params: user,
                    message: "User notifications fetched successfully"
                })
            } else {
                res.status(400).send({
                    "status": "error",
                    "message": "User notifications not found"
                })
            }

        } else {
            res.status(400).send({
                "status": "error",
                "message": "empty user Id"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

router.post('/change-job-status', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        console.log(tokenData.type);
        var creds = req.body;
        if (tokenData.type == 'Administrator') {
            var job = await db.collection('appliedJobs').find({
                "_id": ObjectId(creds._id)
            }).toArray();
            if (job.length == 0) {
                res.status(400).send({
                    "status": "error",
                    "message": "Unfortunately, there is no applied job for this Id"
                });
            } else {
                var change = await db.collection('appliedJobs').update({
                    "_id": ObjectId(creds._id)
                }, {
                        $set: {
                            "status": creds.status
                        }
                    });
                if (change) {
                    res.status(200).send({
                        "status": "ok",
                        "params": '',
                        "message": "Job status has changed"
                    });
                } else {
                    res.status(400).send({
                        "status": "error",
                        "message": "Something went wrong"
                    });
                }
            }
        } else {
            res.status(400).send({
                "status": "error",
                "message": "Sorry, You have not any authority to change the job status"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

router.get('/current_cashbacks', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    try {
        if (tokenData) {
            if (tokenData.type == 'Administrator') {

                var paginate = 0;
                if (req.query['number']) {
                    paginate = (req.query['number'] - 1) * 2; //skip number of entry
                }

                var present_date = moment().format('YYYY-MM-DD');
                var previous_week = moment().subtract(40, 'days').format('YYYY-MM-DD');
                var future_week = moment().add(20, 'days').format('YYYY-MM-DD');
                var query = {
                    $match: {
                        $or: [{
                            $and: [{
                                "cashback_date1":
                                    { $gte: new Date(present_date) }
                            }, {
                                "cashback_status1": false

                            }, { "status": "1" }
                            ]
                        },
                        {
                            $and: [{
                                "cashback_date2":
                                    { $gte: new Date(present_date) }
                            }, {
                                "cashback_status2": false
                            }, { "status": "2" }
                            ]
                        }, {
                            $and: [{
                                "cashback_date3":
                                    { $gte: new Date(present_date) }
                            }, {
                                "cashback_status3": false
                            }, { "status": "3" }]
                        }]
                    }};

                var future = await db.collection('hiringjobs').aggregate([
                    query, {
                        "$lookup": {
                            "from": "users",
                            "localField": "email",
                            "foreignField": "email",
                            "as": "Details"
                        }
                    },
                    { $skip: paginate }, { $limit: 2 }

                ]).toArray();
                var total = await db.collection('hiringjobs').aggregate([
                    query, {
                        "$lookup": {
                            "from": "users",
                            "localField": "email",
                            "foreignField": "email",
                            "as": "Details"
                        }
                    }, { $group: { _id: null, count: { $sum: 1 } } }]).toArray();


                if (future) {
                    res.status(200).send({
                        "status": "ok",
                        "params": {
                            "cashbacks": future,
                            "totalCount": total
                        },
                        "message": "Future cashback details fetched successfully"
                    })
                } else {
                    res.status(400).send({
                        status: "error",
                        message: "Something went wrong"
                    });
                }


            } else {
                res.status(400).send({
                    status: "error",
                    message: "Sorry, You have not any authority to change the job status"
                });
            }
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Token Expired. Please Logout and Login Again"
            });
        }
    } catch (error) {
        res.status(400).send({
            status: "error",
            message:error.message
        })
    }
});

router.get('/completed_cashback', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    try {
        if (tokenData) {
            var paginate = 0;
            if (req.query['number']) {
                paginate = (req.query['number'] - 1) * 2; //skip number of entry
            }
            var query = {
                $match: {
                    $or: [{
                        $and: [{
                            "cashback_status1": true

                        }]
                    }, {
                        $and: [
                        {
                            "cashback_status2": true
                        }]
                    }, {
                        $and: [
                        {
                            "cashback_status3": true
                        }]
                    }]
                }
            };
            var completeCashbacks = await db.collection('hiringjobs').aggregate([
                query, {
                    "$lookup": {
                        "from": "users",
                        "localField": "email",
                        "foreignField": "email",
                        "as": "Details"
                    }
                }, { $skip: paginate }, { $limit: 15 }
            ]).toArray();
            var total = await db.collection('hiringjobs').aggregate([
                query, {
                    "$lookup": {
                        "from": "users",
                        "localField": "email",
                        "foreignField": "email",
                        "as": "Details"
                    }
                }, { $group: { _id: null, count: { $sum: 1 } } }]).toArray();


            res.status(200).send({
                status: "ok",
                params: {
                    "cashbacks": completeCashbacks,
                    "totalCount": total
                },
                message: "Fetched Completed cashbacks"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Token Expired. Please Logout and Login Again"
            });
        }
    } catch (error) {
        res.status(400).send({
            status: "error",
            message: error.message
        })
    }
});

router.get('/pending_cashback', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    try {
        if (tokenData) {
            var paginate = 0;
            if (req.query['number']) {
                paginate = (req.query['number'] - 1) * 15; //skip number of entry
            }
            var present_date = moment().format('YYYY-MM-DD');
            var query = {
                $match: {
                    $or: [{
                        $and: [{
                            "cashback_date1":
                                { $lt: new Date(present_date) }
                        }, {
                            "cashback_status1": false

                        }, { "status": "1" }]
                    }, {
                        $and: [{ "cashback_date2": { $lt: new Date(present_date) } },
                        {
                            "cashback_status2": false
                        }, {
                            "status": 2
                        }]
                    }, {
                        $and: [{ "cashback_date3": { $lte: new Date(present_date) } },
                        {
                            "cashback_status3": false
                        }, {
                            "status": 3
                        }]
                    }]
                }
            };

            var pending = await db.collection('hiringjobs').aggregate([
                query,
                {
                    "$lookup": {
                        "from": "users",
                        "localField": "email",
                        "foreignField": "email",
                        "as": "Details"
                    }
                },
                { $skip: paginate}, { $limit: 15 }
            ]).toArray();

            var total = await db.collection('hiringjobs').aggregate([
                query, { $group: { _id: null, count: { $sum: 1 } } }]).toArray();

            res.status(200).send({
                status: "ok",
                params: {
                    "cashbacks": pending,
                    "totalCount": total
                },
                message: "Fetched pending cashbacks"
            });
        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Token Expired. Please Logout and Login Again"
            });
        }
    } catch (error) {
        res.status(400).send({
            status: "error",
            message: error.message
        });
    }
});




//     ______                      __     ____              __
//    / ____/  ______  ____  _____/ /_   / __ \____  __  __/ /____  _____
//   / __/ | |/_/ __ \/ __ \/ ___/ __/  / /_/ / __ \/ / / / __/ _ \/ ___/
//  / /____>  </ /_/ / /_/ / /  / /_   / _, _/ /_/ / /_/ / /_/  __/ /
// /_____/_/|_/ .___/\____/_/   \__/  /_/ |_|\____/\__,_/\__/\___/_/
//           /_/

module.exports = router;