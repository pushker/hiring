var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var jwt = require('jsonwebtoken');
var axios = require('axios');
var _ = require('lodash');
var helpers = require('../helpers');
var ObjectId = require('mongodb').ObjectId;
var moment = require('moment');
var fs = require('fs');
var nodemailer = require('nodemailer');
const util = require('util');
const options = {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        user: 'dotnet.rev@gmail.com',
        pass: '#rev#123'
    }
};
const transporter = nodemailer.createTransport(options);
//     ____        __        __                       ______                            __  _
//    / __ \____ _/ /_____ _/ /_  ____ _________     / ____/___  ____  ____  ___  _____/ /_(_)___  ____
//   / / / / __ `/ __/ __ `/ __ \/ __ `/ ___/ _ \   / /   / __ \/ __ \/ __ \/ _ \/ ___/ __/ / __ \/ __ \
//  / /_/ / /_/ / /_/ /_/ / /_/ / /_/ (__  )  __/  / /___/ /_/ / / / / / / /  __/ /__/ /_/ / /_/ / / / /
// /_____/\__,_/\__/\__,_/_.___/\__,_/____/\___/   \____/\____/_/ /_/_/ /_/\___/\___/\__/_/\____/_/ /_/

var db;
MongoClient.connect(process.env.MONGO_URL, function (err, client) {
    if (err) {
        throw err;
    }
    db = client.db(process.env.MONGO_DATABASE);
});

//     ____              __
//    / __ \____  __  __/ /____  _____
//   / /_/ / __ \/ / / / __/ _ \/ ___/
//  / _, _/ /_/ / /_/ / /_/  __(__  )
// /_/ |_|\____/\__,_/\__/\___/____/

router.get('/etherRate', async function (req, res, next) {
    try {
        var rate = await helpers.ethereumPrice();
        res.status(200).send({
            "status": "ok",
            "params": rate,
            "message": "Ether rates fetched successfully"
        });
    } catch (err) {
        res.status(400).send({
            "status": "error",
            "params": err
        });
    }
});

router.post('/createWallet', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        try {
            var tokenData = helpers.verifyJWT(req.headers.authorization);
            var walletNew = await helpers.createWallet();
            //var walletExport = await helpers.exportWallet(walletNew.privateKey);
            var check = await db.collection('wallets').find({ "email": tokenData.email }).toArray();
            if (check.length > 0) {
                // var json = {
                //     "username": tokenData.username,
                //     "email": tokenData.email,
                //     "address": walletNew.address,
                //     "private_key": walletNew.privateKey,
                //     "keystore": walletExport.keystore,
                //     "password": walletExport.password
                // };
                // var JsonUrl = await exports.uploadEther(json, tokenData.username);
                var wallet = await db.collection('wallets').update({
                    "email": tokenData.email
                }, {
                        $set: {
                            "address": walletNew.address,
                            "private_key": walletNew.privateKey,
                            "auto_generated": "1",
                            "show":0,
                            "created_at": Date.now()
                        }
                    })

                if (wallet) {
                    res.status(200).send({
                        status: "ok",
                        params: {
                            "address": walletNew.address,
                            "private_key": walletNew.privateKey,
                            "auto_generated": "1",
                            "show": 0
                        },
                        message: "Wallet has updated successfully"
                    });
                } else {
                    throw ({
                        message: "Something went Wrong !!"
                    });
                }
            } else {
                // var jsD = {
                //     "username": tokenData.username,
                //     "email": tokenData.email,
                //     "address": walletNew.address,
                //     "private_key": walletNew.privateKey,
                //     "keystore": walletExport.keystore,
                //     "password": walletExport.password
                // };
                // var JsUrl = await exports.uploadEther(jsD, tokenData.username);
                var walletCreate = await db.collection('wallets').insert({
                    "username": tokenData.username,
                    "email": tokenData.email,
                    "address": walletNew.address,
                    "private_key": walletNew.privateKey,
                    "auto_generated": "1",
                    "show": 0,
                    "created_at": Date.now()
                });
                if (walletCreate) {
                    res.status(200).send({
                        status: "ok",
                        params: {
                            "address": walletNew.address,
                            "private_key": walletNew.privateKey,
                            "auto_generated": "1",
                            "show": 0
                        },
                        message: "wallet has created successfully"
                    });
                } else {
                    throw ({
                        message: "Something Went wrong !!"
                    });
                }
            }

        } catch (err) {
            res.status(400).send({
                status: "error",
                params: err
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }

});
router.get("/balance", async function (req, res, next) {
    try {
        var tokenData = helpers.verifyJWT(req.headers.authorization);
        try {
            var balance = await helpers.balance(req.query.address);

            var updateBal = await db.collection('wallets').update({
                "email": tokenData.email
            }, {
                    $set: {
                        "balance": balance
                    }
                });
            res.status(200).send({
                status: "ok",
                params: balance,
                message: "Fetched balance successfully"
            });
        } catch (error) {
            throw ({
                message: error
            });
        }
    } catch (err) {
        res.status(400).send({
            status: "error",
            params: err
        });
    }
});


router.post('/createAccount', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var creds = req.body;
        if (creds.address) {
            try {
                var check = await db.collection('wallets').find({ "email": tokenData.email }).toArray();
                var checkEtherAddress = await db.collection('wallets').find({ "address": creds.address }).toArray();
                if (check.length == 0 && checkEtherAddress == 0) {
                    var wallet = await db.collection('wallets').insert({
                        "username": tokenData.username,
                        "email": tokenData.email,
                        "address": creds.address,
                        "created_at": Date.now()
                    })

                    if (wallet) {
                        res.status(200).send({
                            status: "ok",
                            params: "",
                            message: "Wallet has created successfully"
                        });
                    } else {
                        throw ({
                            message: "Something went Wrong !!"
                        });
                    }
                } else {
                    res.status(400).send({
                        status: "error",
                        params: "",
                        message: "This address is already allocated to someone, Please enter a unqiue and valid address"
                    });
                }
            } catch (error) {
                throw ({
                    message: error
                });
            }
        } else {
            res.status(400).send({
                status: "error",
                "message": "Empty address"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }

});

router.get('/get-account', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var data = await db.collection('wallets').find({ "email": tokenData.email }).toArray();
        if (data.length > 0) {
            res.status(200).send({
                status: "ok",
                params: data[0],
                message: "Wallet details fetched successfully"
            })
        } else {
            res.status(400).send({
                status: "error",
                message: "User has not any etherem address and account"
            })
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});
/* rejected points code starts here */
router.get('/re-createWallet', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var user = await db.collection('users').find({
            "email": tokenData.email
        }).toArray();
        var wallet = await db.collection('wallets').find({
            "email": tokenData.email
        }).toArray();
        if (user.length > 0 && wallet.length > 0) {
            transporter.sendMail({
                from: "dotnet.rev@gmail.com",
                to: tokenData.email,
                subject: 'Re-generate Etherem wallet',
                html: ` <html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<style type="text/css">

	/* GENERAL EMAIL CLIENT FIXES */
	#outlook a{padding:0;}
	.ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
	.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
	body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}
	table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
	table{border-collapse:collapse !important; table-layout:fixed; !important; margin: 0 auto; !important;}
	table table {table-layout: auto;}
	img{-ms-interpolation-mode:bicubic;}
	body{margin:0; padding:0;}
	img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}

	/* NO MORE BLUE LINKS ON IPHONE */
	.appleLinks a{color: #4B4C4C;text-decoration: none;}
	.appleLinksWhite a{color: #FFFFFF;text-decoration: none;}

	/* MEDIA QUERY SCREEN 414px */
	@-ms-viewport{width:device-width}
	@media only screen and (max-width:414px){
		table[class="container"]{width:100% !important;}
		td[class="text-center"]{text-align: center !important; width:100% !important;}
		td[class="hide"]{display: none !important;}
		table[class="button-width"]{width:100% !important;}
		img[class="crop"]{height:auto !important; max-width:600px !important; width:100% !important;}
	}
	@media screen and (min-width: 601px) {
		table[class="containerDesktop"]{width: 600px!important;}
	}

	@media (min-width: 400px) and (max-width: 418px){
		.innertablesec{
			width: 150%;
		}
	}

</style>
<title>
</title>
</head>
<body>

	<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td style="text-align: center; font-size: 0px; vertical-align: top;">
					<table align="center" border="0" cellspacing="0" cellpadding="0">
						<tbody>
							<tr>
								<td align="center" valign="middle">
									<a>
										<img src="https://s3.amazonaws.com/demodumps/18342a1a-badd-49d2-9750-f552035094de.png" style="width: 100%;">
									</a>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>

	<!-- ADDITIONAL RESOURCES STARTS HERE -->
	<table width="100%" align="center" bgcolor="#ffffff" border="0" cellspacing="0" cellpadding="0" class="innertablesec">
		<tbody>
			<tr>
				<td align="left" valign="top">
	                <table width="100%" align="center" class="containerDesktop" style="max-width: 100%;background: #fff;height:60vh;" border="0" cellspacing="0" cellpadding="0">
	                	<tbody><br>
		            		<tr>
		            			<td align="left" valign="top" style="padding: 0px 20px 20px; color: rgb(75, 76, 76); line-height: 22px !important; font-family: Arial, Helvetica, sans-serif; font-size: 16px;">
		            				<div class="mktEditable" id="Paragraph">
		            					<table width="560" align="center" class="container" border="0" cellspacing="0" cellpadding="0">
		            						<tbody>
	            								<tr>
	            									<td align="left" valign="top" style="color: rgb(75, 76, 76); line-height: 22px; font-family: Arial, Helvetica, sans-serif; font-size: 16px;padding-top: 10%;">
	            									<strong style="font-size: 24px;">Dear ` + tokenData.username + `,</strong>
													<br>
                                                    <p style="font-size: 30px;line-height: 1.5;">Please click this link to re-generate your wallet -
                                                    </p>http://hiring.eprofitanalyzer.com/wallet-setting/` + wallet[0]._id + `
	            									</td>
	            								</tr>
	            							</tbody>
	            						</table>
	            					</div>
	            				</td>
	            			</tr>
							<tr>
								<table align="center" valign="middle" border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td align="center" valign="top" style="font-family: Arial, Helvetica, sans-serif;">
												<div class="mktEditable" id="Footer">
													<table width="100%" align="center" bgcolor="#000" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr align="center" bgcolor="#000">
																<td><br>
																	<p style="color: #fff;">© Ethereum Hiring 2018. All rights reserved. Terms &amp; condition</p>
																	<hr style="width: 50%;">
																</td>
															</tr>
															<tr>
																<td align="center" bgcolor="#000">
																	<table width="600" align="center" class="container" border="0" cellspacing="0" cellpadding="0">
																	<tbody>
																		<tr>
																			<td align="center" valign="middle" style="padding: 16px 20px 10px;">
																				<table align="center" border="0" cellspacing="0" cellpadding="0">
																					<tbody style="text-align: center;">
																					</tbody>
																				</table>
																			</td>
																		</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</tr>
	        			</tbody>
	        		</table>
				</td>
			</tr>
		</tbody>
	</table>


</body>
</html>`,
            }, function (err, resp) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    db.collection('wallets').update({
                        "email": tokenData.email
                    }, {
                            $set: {
                                "account_request": "true"
                            }
                        }, function (error, resop) {
                            if (error) {
                                res.status(400).send({
                                    status: "error",
                                    message: "Something went wrong,Please contact with your support"
                                });
                            } else {
                                res.status(200).send({
                                    status: "ok",
                                    message: "Mail has sent to your E-mail address, Please check your mail, thanks"
                                });
                            }
                        })

                }
            })

        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "You have not the account the old account , so you can not re-create for the timings"
            });
        }

    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

router.post('/regenerate-wallet', async function (req, res, next) {
    var creds = req.body;
    if (creds._id) {
        var check = await db.collection('wallets').find({ "_id": ObjectId(creds._id) }).toArray();
        if (check.length == 0) {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Your reset wallet link is not valid "
            });
        } else {
            if (check[0].account_request == "true") {
                var walletNew = await helpers.createWallet();
                var walletExport = await helpers.exportWallet(walletNew.privateKey);
                var json = {
                    "username": check[0].username,
                    "email": check[0].email,
                    "address": walletNew.address,
                    "private_key": walletNew.privateKey,
                    "keystore": walletExport.keystore,
                    "password": walletExport.password
                };
                var JsonUrl = await exports.uploadEther(json, check[0].username);
                var wallet = await db.collection('wallets').update({
                    "email": check[0].email
                }, {
                        $set: {
                            "address": walletNew.address,
                            "private_key": walletNew.privateKey,
                            "keystore": walletExport.keystore,
                            "password": walletExport.password,
                            "auto_generated": "re-generated",
                            "url": JsonUrl,
                            "account_request":"false",
                            "created_at": Date.now()
                        }
                    });

                if (wallet) {
                    res.status(200).send({
                        status: "ok",
                        params: {
                            "address": walletNew.address,
                            "password": walletExport.password,
                            "private_key": walletNew.privateKey,
                            "keystore": walletExport.keystore,
                            "auto_generated": "re-generated",
                            "url": JsonUrl
                        },
                        message: "Wallet has updated successfully"
                    });
                } else {
                    res.status(400).send({
                        "status": "error",
                        "message": "Your wallet has not updated"
                    });
                }
            } else {
                res.status(400).send({
                    "status": "error",
                    "params": "",
                    "message": "You have not the permission to re-generate the ether wallet"
                });
             }
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "You have not the permission to re-generate the ether wallet"
        });
    }
});
/* rejected points code ends here */
router.get('/statusWallet', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var wallet = await db.collection('wallets').find({
            "email": tokenData.email
        }).toArray();
        if (wallet) {
            var updateStatus = await db.collection('wallets').update({
                "email": tokenData.email
            }, {
                    $set: {
                        "show": 1
                    }
                });
            if (updateStatus) {
                res.status(200).send({
                    "status": "ok",
                    "message": "wallet auto-generate status updated"
                });
            } else {
                res.status(400).send({
                    "status": "error",
                    "message": "Unable to update the status of wallet"
                });
            }
        } else {
            res.status(400).send({
                "status": "error",
                "message": "Unable to find your existing wallet address to update the status"
            });
        }
    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});

//send private key to user
router.get('/praivateKeyMail', async function (req, res, next) {
    var tokenData = helpers.verifyJWT(req.headers.authorization);
    if (tokenData) {
        var user = await db.collection('users').find({
            "email": tokenData.email
        }).toArray();
        var wallet = await db.collection('wallets').find({
            "email": tokenData.email
        }).toArray();
        if (user.length > 0 && wallet.length > 0 && wallet[0].private_key !='') {
            transporter.sendMail({
                from: "dotnet.rev@gmail.com",
                to: tokenData.email,
                subject: 'Etherem Private Key',
                html: ` <html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<style type="text/css">

	/* GENERAL EMAIL CLIENT FIXES */
	#outlook a{padding:0;}
	.ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
	.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
	body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}
	table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
	table{border-collapse:collapse !important; table-layout:fixed; !important; margin: 0 auto; !important;}
	table table {table-layout: auto;}
	img{-ms-interpolation-mode:bicubic;}
	body{margin:0; padding:0;}
	img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}

	/* NO MORE BLUE LINKS ON IPHONE */
	.appleLinks a{color: #4B4C4C;text-decoration: none;}
	.appleLinksWhite a{color: #FFFFFF;text-decoration: none;}

	/* MEDIA QUERY SCREEN 414px */
	@-ms-viewport{width:device-width}
	@media only screen and (max-width:414px){
		table[class="container"]{width:100% !important;}
		td[class="text-center"]{text-align: center !important; width:100% !important;}
		td[class="hide"]{display: none !important;}
		table[class="button-width"]{width:100% !important;}
		img[class="crop"]{height:auto !important; max-width:600px !important; width:100% !important;}
	}
	@media screen and (min-width: 601px) {
		table[class="containerDesktop"]{width: 600px!important;}
	}

	@media (min-width: 400px) and (max-width: 418px){
		.innertablesec{
			width: 150%;
		}
	}

</style>
<title>
</title>
</head>
<body>

	<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td style="text-align: center; font-size: 0px; vertical-align: top;">
					<table align="center" border="0" cellspacing="0" cellpadding="0">
						<tbody>
							<tr>
								<td align="center" valign="middle">
									<a>
										<img src="https://s3.amazonaws.com/demodumps/18342a1a-badd-49d2-9750-f552035094de.png" style="width: 100%;">
									</a>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>

	<!-- ADDITIONAL RESOURCES STARTS HERE -->
	<table width="100%" align="center" bgcolor="#ffffff" border="0" cellspacing="0" cellpadding="0" class="innertablesec">
		<tbody>
			<tr>
				<td align="left" valign="top">
	                <table width="100%" align="center" class="containerDesktop" style="max-width: 100%;background: #fff;height:60vh;" border="0" cellspacing="0" cellpadding="0">
	                	<tbody><br>
		            		<tr>
		            			<td align="left" valign="top" style="padding: 0px 20px 20px; color: rgb(75, 76, 76); line-height: 22px !important; font-family: Arial, Helvetica, sans-serif; font-size: 16px;">
		            				<div class="mktEditable" id="Paragraph">
		            					<table width="560" align="center" class="container" border="0" cellspacing="0" cellpadding="0">
		            						<tbody>
	            								<tr>
	            									<td align="left" valign="top" style="color: rgb(75, 76, 76); line-height: 22px; font-family: Arial, Helvetica, sans-serif; font-size: 16px;padding-top: 10%;">
	            									<strong style="font-size: 24px;">Dear ` + tokenData.username + `,</strong>
													<br>
                                                    <p style="font-size: 30px;line-height: 1.5;">Here below is your etherem private key of your wallet -
                                                    </p>` + wallet[0].private_key + `
                                                    <p>Please copy your key and store at a safe place.</p>
	            									</td>
	            								</tr>
	            							</tbody>
	            						</table>
	            					</div>
	            				</td>
	            			</tr>
							<tr>
								<table align="center" valign="middle" border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td align="center" valign="top" style="font-family: Arial, Helvetica, sans-serif;">
												<div class="mktEditable" id="Footer">
													<table width="100%" align="center" bgcolor="#000" border="0" cellspacing="0" cellpadding="0">
														<tbody>
															<tr align="center" bgcolor="#000">
																<td><br>
																	<p style="color: #fff;">© Ethereum Hiring 2018. All rights reserved. Terms &amp; condition</p>
																	<hr style="width: 50%;">
																</td>
															</tr>
															<tr>
																<td align="center" bgcolor="#000">
																	<table width="600" align="center" class="container" border="0" cellspacing="0" cellpadding="0">
																	<tbody>
																		<tr>
																			<td align="center" valign="middle" style="padding: 16px 20px 10px;">
																				<table align="center" border="0" cellspacing="0" cellpadding="0">
																					<tbody style="text-align: center;">
																					</tbody>
																				</table>
																			</td>
																		</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</tr>
	        			</tbody>
	        		</table>
				</td>
			</tr>
		</tbody>
	</table>


</body>
</html>`,
            }, function (err, resp) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    res.status(200).send({
                        status: "ok",
                        message: "Mail has sent to your E-mail address, Please check your mail, thanks"
                    });

                }
            })

        } else {
            res.status(400).send({
                "status": "error",
                "params": "",
                "message": "Sorry,You have not the private key of your etherem address"
            });
        }

    } else {
        res.status(400).send({
            "status": "error",
            "params": "",
            "message": "Token Expired. Please Logout and Login Again"
        });
    }
});
//helper function
exports.uploadEther = async function (jsonData, username) {
    try {
        let buff = JSON.stringify(jsonData, null, 2);
        var filename = await Date.now() + username + "." + 'json';
        var file = await fs.writeFileSync(process.env.FILEPATHJSON + "/" + filename, buff, 'utf-8');
        return process.env.WEBURLJSON + "/" + filename;
    } catch (error) {
        return "Corruted Json";
    }
}

/* ethereum api*/
module.exports = router;